(function($){

  $.fn.currDateTime = function(){
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;
    return dateTime;
  };

  $.fn.my_date_format = function(input){
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    var time = d.toLocaleTimeString().toLowerCase().replace(/([\d]+:[\d]+):[\d]+(\s\w+)/g, "$1$2");
    return (date + " " + time);
    };

  $.fn.clickableModal2 = function(){
    $('body').on('click', '#postBox .card.content', function(){
      var idpost = $(this).attr('id');
      $(this).showPostBySearch(idpost);
    });
  };

  $.fn.shortingPost = function(){
    $('.wrapper').fadeIn('slow');
    $('#main-card').html($('#postBox').children().first());
    $('#postBox div.d-none[name=fullContent]').each(function(){
      $(this).siblings('[name=shortContent]').ellipsis({ row:2 });
    });
  };

  $.fn.adjustBodyHeight = function(){
    var currHeight = $('#changeable').height();
    var newHeight = currHeight + 230;
    var sideBar = $('#navbarToggleExternalContent');
    var content = $('#menu-home');
    if(currHeight < 500){
      sideBar.css('height', 600);
      content.css('height', 600);
    }else{
      sideBar.css('height', newHeight);
      content.css('height', newHeight);
    }
  };

  jQuery.ajaxSetup({
    beforeSend: function() {
      $('#loadingDiv').fadeIn('fast');
    },
    success: function(){
      $('#loadingDiv').fadeOut('fast');
    },
    complete: function(){
      $('#loadingDiv').fadeOut('slow');  
      $(this).adjustBodyHeight();
    }
  });

  $.fn.ajaxRemovePostModalResponse = function(response){
    if(response==1){
        var $response = $('#modalResponsePostSuccess .modal-content');
        $('#modalConfirmRemovePost .modal-content').html($response);
        $('#modalConfirmRemovePost').on('hide.bs.modal', function(){ location.reload(); });
      }else{
        var $response = $('#modalResponsePostFail .modal-content');
        $('#modalConfirmRemovePost .modal-content').html($response);
        $('#modalConfirmRemovePost').on('hide.bs.modal', function(){ location.reload(); });
      }
  }

  $.fn.titleChange = function(){
    $('#postedTitle').on('keydown', function(){
        var titleEmpty = jQuery.isEmptyObject($(this).val()) ? $(this).addClass('bg-danger') : $(this).removeClass('bg-danger');
    });
  };

  $.fn.validateLogin = function(){
    var valid = false;
    var username = $('#form_login input[name=username]').val();
    var password = $('#form_login input[name=password]').val();
    if(username !== '' && password !== ''){ valid = true; }
    return valid;
  }

  $.fn.validateForm = function(){
    var valid = true;
    if( jQuery.isEmptyObject($('#postedTitle').val()) ){
      valid = false;
      $('#postedTitle').addClass('bg-danger');
    } 
    if( jQuery.isEmptyObject($.trim($('#summernote').summernote('code'))) ) {
      valid = false;
      alert('you cannot post empty message');
    }  
    if(valid){ return true; }
    return false;
  };

  $.fn.showBodyPost = function(){
    if($('#menu-post').css('display') == 'none' ){
      $('#menu-home').fadeOut('50');
      $('#menu-post').fadeIn(1000).css('display','block');
    }
    if($('.midResp').css('display') == 'none'){ $('.midResp').fadeIn(); }
  };

  $.fn.filterPostPublished = function(){
    $('div.card.mb-3').each(function(index){
      var show = $(this).attr('type') == 1 ? $(this).fadeIn(1000) : $(this).fadeOut(100);
    });
  };

  $.fn.filterPostDraft = function(){
    $('div.card.mb-3').each(function(index){
      var show = $(this).attr('type') == 0 ? $(this).fadeIn(1000) : $(this).fadeOut('slow');
    });
  };

  $.fn.filterPostAll = function(){
    $('div.card.mb-3').each(function(index){
      var show = $(this).css('display') == 'none' ? $(this).fadeIn(1000) : '';
    });
  };

  $.fn.fCountAllPost = function(){
    $('#badgeAllPost').text($('#cAllPost').children().find('div.card.mb-3').length);
  };

  $.fn.fCountPublishedPost = function(){
    $('#badgePublishedPost').text($('#cAllPost').children().find('div[type=1]').length);
  };

  $.fn.fCountDraft = function(){
    $('#badgeDraft').text($('#cAllPost').children().find('div[type=0]').length);
  };

  $.fn.my_date_format = function(input){
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var date = d.getDate() + " " + month[d.getMonth()] + ", " + d.getFullYear();
    var time = d.toLocaleTimeString().toLowerCase().replace(/([\d]+:[\d]+):[\d]+(\s\w+)/g, "$1$2");
    return (date + " " + time);
    };

  $.fn.loadMorePost = function(shown, el){
    $.ajax({
      dataType: "json",
      url: 'Auth/loadMorePost',
      data: {'from' : shown},
      success: function(data){
        var html = '';
        $.each(data, function(key, value){
            html += '<div class="card content p-3" id='+value.c_idPost+'>';
            if(value.c_urlAttachment != ''){ html += '<img class="card-img-top" src="'+value.c_urlAttachment+'">'; }
            html += '<div class="card-body -pb mc-hover-white">';
            html += '<h5 class="card-title font-weight-bold">'+value.c_postTitle+'</h5>';
            html += '<div class="card-text" name="shortContent">'+value.c_postContent+'</div>';
            html += '<div class="d-none" name="fullContent">'+value.c_postContent+'</div>';
            html += '<footer class="blockquote-footer">';
            html += '<span class="text-muted mr-2" title="author">'+value.c_divisi+', </span>';
            html += '<span class="text-muted" title="author">'+value.c_nama+' </span></br>';
            if(value.c_isModified == 0){ html += '<cite>posted '+$(this).my_date_format(value.c_postDate)+'</cite>';
          }else{ html += '<cite>updated '+$(this).my_date_format(value.c_postModifiedDate)+'</cite>'; }
            html += '</footer></div></div>';
        });
        el.append(html);
      }
    }).done(function(){
      $('#postBox div.d-none[name=fullContent]').each(function(){
        $(this).siblings('[name=shortContent]').ellipsis({ row:2 });
      });
    });
  };

  // instantiate tooltip inside post
  $.fn.instantiateTooltips = function(){
    $('#list_post').on('load', function(){
      $hovers = $(this).children().find('.menu-hover');
      $hovers.hover(function(){
        $(this).tooltip({'placement' : 'bottom' },'show');
      }, function(){
        $(this).tooltip('hide');
      })
    });
  }

}(jQuery));