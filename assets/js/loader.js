jQuery.ajaxSetup({
  beforeSend: function() {
  	$("#hidden").css('display', 'block');
	$("#progressbar2").stop().animate({width:"100%"}, 1000, function() {
		$("#hidden").css('display', 'none');
		$("#overlay").fadeIn("slow");
	});
  },
  complete: function(){
  },
  success: function(){
  	$("#progressbar2").animate({width:"100%"}, 100, function(){
	});
  }
});
