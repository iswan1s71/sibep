<?php 
require_once(APPPATH."controllers/Welcome.php");
Class V2 extends Welcome {
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('file');
        $this->load->model('Mlogin');
        $this->load->model('MUser');
        $this->load->helper('directory');
        $this->load->helper('url');
      }
      
    public function index(){
        if(!isset($this->session->userdata['logged_in'])){
            redirect('');
        }else{
            $data['akun'] = $this->MUser->mGetDataPengguna($this->session->userdata['logged_in']['iduser']);
            $data['maxPost'] = $this->MUser->mGetMaxPost($this->session->userdata['logged_in']['iduser']);
            //$data['posts'] = $this->MUser->mGetAllPostByIDUser($this->session->userdata['logged_in']['iduser']);
            //$this->load->view('User/Home', $data);
            $data['sPost'] = $this->Mlogin->mGetViewPublicPostInitLatest();
            //$this->load->view('Beranda', $data);
            $this->load->view('User/index', $data);
        }
    }

    public function initEditingPost(){
        /* using get instead of post */
        $this->isSessionActive();
        $idPost = $this->input->get('idPost');
        $query = $this->MUser->mGetPostByIDPostObj($idPost);
        foreach($query->result() as $object){
            $result['object'] = $object;
        }
        $this->load->view('User/module/posts/new', $result);
    }

    public function getPostById(){
        if($this->input->get('idPost') == null){
            die('ERRRRR');
        }else{
            echo json_encode($this->Mlogin->mGetPostById($this->input->get('idPost')));
        }
    }

    public function removePost(){  
        /* using get instead of post */
        $this->isSessionActive();
        $idPost = $this->input->get('idPost');
        //$result = $this->MUser->mHidePostReturn($idPost);
        $result = $this->MUser->mRemovePost($idPost);
        echo $result;
    }

    public function allPost(){
        if(!isset($this->session->userdata['logged_in'])){
            redirect('/');
        }else{
            $data['posts'] = $this->MUser->mGetAllPostByIDUser($this->session->userdata['logged_in']['iduser']);
            $this->load->view('User/module/posts/posts', $data);
        }
    }

    public function loadUserPostPage(){
        if(!isset($this->session->userdata['logged_in'])){
            redirect('/');
        }else{
            $data['fpost'] = $this->MUser->mGetIndexAllPostByIDUser($this->session->userdata['logged_in']['iduser']);
            $data['maxPost'] = $this->MUser->mGetMaxPost($this->session->userdata['logged_in']['iduser']);
            $this->load->view('User/module/posts/posts', $data);
        }
    }

    public function fetchPost(){
        if(!isset($this->session->userdata['logged_in'])){
            redirect('/');
        }else{
            $limitStart = $this->input->get('page');
            if(!isset($limitStart)) {
                $limitStart = 1;
            }
            $data['posts'] = $this->MUser->mGetLimitPostByIDUser($this->session->userdata['logged_in']['iduser'], $limitStart);
            $data['maxPost'] = $this->MUser->mGetMaxPost($this->session->userdata['logged_in']['iduser']);
            $this->load->view('User/module/posts/listPostData', $data);
        }
    }

    public function Posting(){
        $this->isSessionActive();
          $data['iduser'] = $this->input->post('iduser');
          $data['postedTitle'] = $this->input->post('postedTitle');
          $content = $this->input->post('postedText');
          $contentStr = str_replace("'", "", $content);
          $data['postedText'] = $contentStr;
          date_default_timezone_set('Asia/Jakarta');
          $postDate = date('Y-m-d H:i:s');
          $data['postDate'] = $postDate;
          $data['hasAttachment'] = 0;
          $data['urlAttachment'] = '';
          $reff=($this->input->post('reff')=='publish' ? 1 : 0);
          $data['isPublish'] = $reff;
          $result = $this->MUser->mSavePosting($data); 
          echo $result ;
      }

      public function PostingUpdate(){
        $this->isSessionActive();
          $data['iduser'] = $this->session->userdata['logged_in']['iduser'];
          $data['idpost'] = $this->input->post('idpost');
          $data['postedTitle'] = $this->input->post('postedTitle');
          $data['postedText'] = $this->input->post('postedText');
          //gen date time for //
          date_default_timezone_set('Asia/Jakarta');
          $postDate = date('Y-m-d H:i:s');
          $data['dateModifed'] = $postDate;
          //var_dump($data);
          $this->MUser->mUpdatePosting($data);
      }

      public function profile(){
          $this->isSessionActive();
          $data['user'] = $this->MUser->mGetDataPengguna($this->session->userdata['logged_in']['iduser']);
          $this->load->view('User/module/profile/profile', $data);
      }
}
?>