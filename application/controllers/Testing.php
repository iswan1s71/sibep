<?php 

Class Testing extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $data['right'] = new RightHand();
        $this->load->view('Test/index', $data);
    }

}


interface Mouse {

    public function leftClick();

    public function rightClick();

    public function scroll();

}

Class RightHand implements Mouse {

    public function leftClick(){
        return 'left click';
    }

    public function rightClick(){
        return 'right click';
    }

    public function scroll(){
        return 'scroll function';
    }

}



?>