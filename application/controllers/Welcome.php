<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('file');
		$this->load->model('Mlogin');
		$this->load->model('MAdmin');
		$this->load->helper('download');
	}

	public function index(){
		if(isset($this->session->userdata['logged_in'])){
		  $this->sethomepage($_SESSION['logged_in']['level']);
		}else{
		  $data['sPost'] = $this->Mlogin->mGetViewPublicPost();
		  $this->load->view('Beranda', $data);
		}
	}

	private function sethomepage($level){
    switch($level){
      case '1':
        redirect('Admin');
        break;
      case '2':
        redirect('V2');
        break;
      default:
        redirect('Welcome');
        break;
    }
  }

	public function searchpage(){
		$data['sPost'] = $this->Mlogin->mGetViewPublicPost();
		$this->load->view('common_template/searchpage', $data);
	}

	public function loginpage(){
		$this->load->view('common_template/loginpage');
	}

	public function register(){
		$data['datauser'] = $this->fGetUserList();
		$this->load->view('common_template/registerpage', $data);
	}

	public function download(){
		$this->load->view('common_template/downloadpage');
	}

	private function checkNoParams(){
		if(!isset($_POST)) die('no params');
	}

	public function downloader(){
		$this->checkNoParams();
		$name = $this->input->post('name');
		$type = $this->input->post('type');

		switch ($name){
			case 'sibep-apk':
				$mimeType = 'application/vnd.android.package-archive';
				$strFile = 'assets/downloads/i-leibel.apk';
				$strFileName = 'sibep.apk';

				header("HTTP/1.1 200 OK");
				header("Pragma: public");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private", false);

				header("Content-type: " . $mimeType);

				header("Content-Disposition: attachment; filename=\".$strFileName."); 

				header("Content-Transfer-Encoding: binary");

				header("Content-Length: " . mb_strlen($strFile));

				echo $strFile;
			break;

			default:
			break;
		}
	}

	public function downloadApk(){
		//die("./libraries/certs/cacert.pem");
		$arrContextOptions=array(
			"ssl"=>array(
				"cafile" => base_url("assets/certs/cacert.pem"),
				"verify_peer"=> false,
				"verify_peer_name"=> false,
			),
		);

		header("Content-Disposition: attachment; filename=sibep.apk");
		
		$response = file_get_contents(base_url('assets/downloads/sibep.apk'), 0, stream_context_create($arrContextOptions));
		echo $response;
	}

	public function isSessionActive(){
		if(!isset($this->session->userdata['logged_in'])){
			die(redirect('Auth'));
		}
	}
	
	/* register section */

	public function fGetUserList() {
		$query = $this->MAdmin->mGetUserList();
		if (empty($query)) { return false; }
		foreach ( $query as $key => $val ) {
			$kolom = array_values($val);
			$user['id'][] = $kolom[0];
			$user['nama'][] = $kolom[1];
		}
		return $user;
	}

	public function fGetDivisionForRegister(){
		$id = $this->input->get('idkaryawan');
		$sql = 'SELECT * FROM tbl_user WHERE c_iduser="' . $id . '";';
		$query = $this->db->query($sql);
		// $row = $query->row();
		// echo $row->c_divisi;
		header('Content-Type: application/json');
    	echo json_encode( $query->result_array() );
	}

	public function registerUser(){
		$data['iduser'] = $this->input->post('iduser');
		$data['username'] = $this->input->post('username');
		$data['password'] = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
		$query = $this->Mlogin->mUsernameExist($data['username']);
		if(!empty($query)){
			die('exist');
		}
		$register = $this->Mlogin->mRegisterNewLogin($data);
		if($register == 'success'){
			echo 'success';
		}else{
			die('ada error registering');
		}
	}
	
  public function myphp(){
		phpinfo();
	}
	
}
