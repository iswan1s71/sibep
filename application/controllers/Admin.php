<?php
require_once(APPPATH."controllers/Welcome.php");
Class Admin extends CI_Controller{

  public function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('file');
    $this->load->model('MAdmin');
  }

  public function index(){
    if(isset($this->session->userdata['logged_in'])){
      $data['datalogin'] = $this->fGetUserLogin();
      $data['datauser'] = $this->fGetUserList();
      $this->load->view('Admin/Home', $data);
    }else{
      $this->load->view('Beranda');
    }
  }

  private function fGetUserLogin(){
    $datauser = $this->MAdmin->mGetUserLogin();
    return $datauser->result_array();
  }

  public function fGetUserList() {
			$query = $this->MAdmin->mGetUserList();
			if (empty($query)) { return false; }
			foreach ( $query as $key => $val ) {
				$kolom = array_values($val);
				$user['id'][] = $kolom[0];
				$user['nama'][] = $kolom[1];
			}
			return $user;
	}

  public function fInitUpdateUser() {
    if (isset($_POST['iduser']) === '' ) {
      $iduser = '';
    } else {
      $query = $this->MAdmin->mGetUserById($_POST['iduser']);
      $data = array(
        'iduser' => $query[0]->c_iduser,
        'username' => $query[0]->c_username,
        'password' => $query[0]->c_password,
        'level' => $query[0]->c_level
        );
      $data['data'] = $data;
      $this->load->view('admin/admmodalupdate',$data);
    }
  }

  public function fUpdateUser() {
		if (isset($_POST['iduser']) === '' ) {
			exit;
		} else {
		$iduser = $_POST['iduser'];
		$username = $_POST['username'];
		$password = password_hash($_POST['password'], PASSWORD_DEFAULT);
		$level = '2';
		$hasil = $this->MAdmin->mUpdateUser($iduser, $username, $password, $level);
		if ($hasil != false) {
				echo '<div class="jumbotron" style="background-color:#00CC66">
					<h3 class="cl1 text-center">iduser : ' . $iduser . '<br/></h3>
					<h2 class="cl1 text-center">user berhasil diupdate</h2>
				</div>';
			} else {
				echo '<div class="jumbotron" style="background-color:#CCCC00">
					<h3 class="cl1 text-center">iduser : ' . $iduser . '<br/></h3>
					<h2 class="cl1 text-center">user gagal update</h2>
				</div>';
			}
		}
	}

  public function fAddNewUser() {
    $iduser = $_POST['iduser'];
    $username = $_POST['username'];
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $level = $_POST['level'];
    $berhasil = $this->MAdmin->mAddNewUser($iduser, $username, $password, $level);

    if ($berhasil != false) {
      echo '<div class="jumbotron" style="background-color:#00CC66">
          <h2 class="cl1 text-center">user berhasil ditambahkan</h2>
        </div>';
    } else {
      echo '<div class="jumbotron" style="background-color:#CCCC00">
          <h2 class="text-warning text-center">IDUSER SUDAH TERDAFTAR<br/>
          <small>periksa lagi isiannya broo...!!!</small></h2>
        </div>';
    }
  }

  public function fDelUser() {
		$iduser = $_POST['iduser'];
		$hasil = $this->MAdmin->mDelUser($iduser);
		if ($hasil != false) {
			echo '<div class="jumbotron" style="background-color:#CCCC00">
					<h2 class="cl1 text-center">user berhasil dihapus</h2>
				</div>';
		} else {
			echo '<div class="jumbotron" style="background-color:#CCCC00">
					<h2 class="text-warning text-center">Gagal hapus user<br/>
					<small>periksa lagi isiannya broo...!!!</small></h2>
				</div>';
		}
	}

}
?>
