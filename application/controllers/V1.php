<?php 
require_once(APPPATH."controllers/User.php");
Class V1 extends User {
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('file');
        $this->load->model('Mlogin');
        $this->load->model('MUser');
        $this->load->helper('directory');
        $this->load->helper('url');
      }
    public function index(){
        if(!isset($this->session->userdata['logged_in'])){
            redirect('');
        }else{
            $data['akun'] = $this->MUser->mGetDataPengguna($this->session->userdata['logged_in']['iduser']);
            $data['posts'] = $this->MUser->mGetAllPostByIDUser($this->session->userdata['logged_in']['iduser']);
            //$this->load->view('User/Home', $data);
            $data['sPost'] = $this->Mlogin->mGetViewPublicPost();
            //$this->load->view('Beranda', $data);
            $this->load->view('User/v1/index', $data);
        }
    }

    public function removePost(){  
        /* using get instead of post */
        $this->isSessionActive();
        $idPost = $this->input->get('idPost');
        $result = $this->MUser->mHidePostReturn($idPost);
        echo $result;
    }
}
?>