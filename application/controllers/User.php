<?php
require_once(APPPATH."controllers/Welcome.php");
Class User extends CI_Controller{

  public function __construct() {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('file');
    $this->load->model('Mlogin');
    $this->load->model('MUser');
    $this->load->helper('directory');
  }

  public function index(){
    if(!isset($this->session->userdata['logged_in'])){
      $this->load->view('Beranda');
    }else{
      $data['akun'] = $this->MUser->mGetDataPengguna($this->session->userdata['logged_in']['iduser']);
      //$data['posts'] = $this->MUser->mGetAllPostByIDUser($this->session->userdata['logged_in']['iduser']);
      //$this->load->view('User/Home', $data);
      $data['sPost'] = $this->Mlogin->mGetViewPublicPost();
      //$this->load->view('Beranda', $data);
      $this->load->view('User/v1/distribution/index', $data);
    }
  }

  public function isSessionActive(){
    if(!isset($this->session->userdata['logged_in'])){
      die(redirect('Auth'));
    }
  }

  public function Posts(){
      $this->isSessionActive();
      $data['sPost'] = $this->Mlogin->mGetViewPublicPost();
      $data['akun'] = $this->MUser->mGetDataPengguna($this->session->userdata['logged_in']['iduser']);
      $data['posts'] = $this->MUser->mGetAllPostByIDUser($this->session->userdata['logged_in']['iduser']);
      $this->load->view('User/Posts', $data);
  }

  private function base64ToImage($base64_string, $output_file) {
    $file = fopen($output_file, "wb");
    $data = explode(',', $base64_string);
    fwrite($file, base64_decode($data[1]));
    fclose($file);
    return $output_file;
  }

  public function Posting(){
    $this->isSessionActive();
      $data['iduser'] = $this->input->post('iduser');
      $data['postedTitle'] = $this->input->post('postedTitle');
      $data['postedText'] = $this->input->post('postedText');
      date_default_timezone_set('Asia/Jakarta');
      $postDate = date('Y-m-d H:i:s');
      $data['postDate'] = $postDate;
      $data['hasAttachment'] = 0;
      $data['urlAttachment'] = '';
      $reff=($this->input->post('reff')=='publish' ? 1 : 0);
      $data['isPublish'] = $reff;

      if( $this->input->post('file') !== null ) {
        //uploading image
        $destDir = 'assets/img/' . $data['iduser'] . '/' . date('YmdHis');
        $destFile = $destDir . '/' . $_FILES['file']['name'];
        $imgFileType = pathinfo($destFile, PATHINFO_EXTENSION);
        $uploadOK = 1;
        if(!is_dir($destDir)){
          mkdir($destDir, 0700, true);
        }
        if($imgFileType != 'jpg' && $imgFileType != 'png' && $imgFileType != 'jpeg' && $imgFileType != 'gif'){
          $uploadOK = 0;
        }
        if($uploadOK == 0){
          echo 'upload gambar error';
        }else{
          $isjpg = $imgFileType . 'jpg';
          $ispng = $imgFileType . 'png';
          $isjpeg = $imgFileType . 'jpeg';
          $isgif = $imgFileType . 'gif';

          if (file_exists($isjpeg)) {
              unlink($imgFileType);
              rename($isjpeg, $imgFileType . '.jpeg.old'); }
          if (file_exists($isjpg)) {
              unlink($imgFileType . 'jpg');
              rename($isjpg, $imgFileType . '.jpg.old'); }
          if (file_exists($ispng)) {
              unlink($imgFileType . 'png');
              rename($ispng, $imgFileType . '.png.old'); }
          if (file_exists($isgif)) {
              unlink($imgFileType . 'gif');
              rename($isgif, $imgFileType . '.gif.old'); }
          }
          move_uploaded_file($_FILES['file']['tmp_name'], $destFile);
          // $myimg = $destFile . $imgFileType;
          // make thumbnails
          $this->makeThumbnails($destDir . "/", $_FILES['file']['name']);
          $data['hasAttachment'] = 1;
          $data['urlAttachment'] = $destDir . "/" . $_FILES['file']['name'];
        //end uploading image
      }
      $result = $this->MUser->mSavePosting($data);  
    return $result;
  }

  private function makeThumbnails($updir, $img)	{
	    $thumbnail_width = 128;
	    $thumbnail_height = 128;
	    $thumb_beforeword = "thumb_";
	    $thumbx = $thumb_beforeword. $img ;
	    $arr_image_details = getimagesize("$updir" . "$img");
	    $original_width = $arr_image_details[0];
	    $original_height = $arr_image_details[1];
	    if ($original_width > $original_height) {
	        $new_width = $thumbnail_width;
	        $new_height = intval($original_height * $new_width / $original_width);
	    } else {
	        $new_height = $thumbnail_height;
	        $new_width = intval($original_width * $new_height / $original_height);
	    }
	    $dest_x = intval(($thumbnail_width - $new_width) / 2);
	    $dest_y = intval(($thumbnail_height - $new_height) / 2);
	    if ($arr_image_details[2] == IMAGETYPE_GIF) {
	        $imgt = "ImageGIF";
	        $imgcreatefrom = "ImageCreateFromGIF";
	    }
	    if ($arr_image_details[2] == IMAGETYPE_JPEG) {
	        $imgt = "ImageJPEG";
	        $imgcreatefrom = "ImageCreateFromJPEG";
	    }
	    if ($arr_image_details[2] == IMAGETYPE_PNG) {
	        $imgt = "ImagePNG";
	        $imgcreatefrom = "ImageCreateFromPNG";
	    }
	    if ($imgt) {
	        $old_image = $imgcreatefrom("$updir" . "$img");
	        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	        imagefill($new_image,0,0,0x7fffffff);
	        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
	        $imgt($new_image, "$updir" . "$thumbx");
	    }
	}

  public function PostingUpdate(){
    $this->isSessionActive();
      $data['iduser'] = $this->session->userdata['logged_in']['iduser'];
      $data['idPost'] = $this->input->post('idPost');
      $data['postedTitle'] = $this->input->post('postedTitle');
      $data['postedText'] = $this->input->post('postedText');
      //gen date time for //
      date_default_timezone_set('Asia/Jakarta');
      $postDate = date('Y-m-d H:i:s');
      $data['dateModifed'] = $postDate;
      if($this->input->post('imgChanged') == '1') {
        //uploading image
        $destDir = 'assets/img/' . $data['iduser'] . '/' . date('YmdHis');
        $destFile = $destDir . '/' . $_FILES['file']['name'];
        $imgFileType = pathinfo($destFile, PATHINFO_EXTENSION);
        $uploadOK = 1;
        if(!is_dir($destDir)){
          mkdir($destDir, 0700, true);
        }
        if($imgFileType != 'jpg' && $imgFileType != 'png' && $imgFileType != 'jpeg' && $imgFileType != 'gif'){
          $uploadOK = 0;
        }
        if($uploadOK == 0){
          echo 'upload gambar error';
        }else{
          $isjpg = $imgFileType . 'jpg';
          $ispng = $imgFileType . 'png';
          $isjpeg = $imgFileType . 'jpeg';
          $isgif = $imgFileType . 'gif';
          if (file_exists($isjpeg)) {
              unlink($imgFileType);
              rename($isjpeg, $imgFileType . '.jpeg.old'); }
          if (file_exists($isjpg)) {
              unlink($imgFileType . 'jpg');
              rename($isjpg, $imgFileType . '.jpg.old'); }
          if (file_exists($ispng)) {
              unlink($imgFileType . 'png');
              rename($ispng, $imgFileType . '.png.old'); }
          if (file_exists($isgif)) {
              unlink($imgFileType . 'gif');
              rename($isgif, $imgFileType . '.gif.old'); }
          }
          move_uploaded_file($_FILES['file']['tmp_name'], $destFile);
          // $myimg = $destFile . $imgFileType;
          // make thumbnails
          $this->makeThumbnails($destDir . "/", $_FILES['file']['name']);
          $data['hasAttachment'] = 1;
          $data['urlAttachment'] = $destDir . "/" . $_FILES['file']['name'];
        //end uploading image
      }
      if($this->input->post('imgChanged') == '2'){
        $data['hasAttachment'] = 0;
        $data['urlAttachment'] = "";
      }
      $this->MUser->mUpdatePosting($data);
  }

  public function removePost(){
    $this->isSessionActive();
      $idPost = $this->input->post('idPost');
      $this->MUser->mHidePost($idPost);
  }

  public function makePublish(){
    $this->isSessionActive();
      $idPost = $this->input->post('idPost');
      $this->MUser->mMakePublish($idPost);
  }

  public function makeDraft(){
    $this->isSessionActive();
      $idPost = $this->input->post('idPost');
      $this->MUser->mMakeDraft($idPost);
  }

  private function getAllPostByID($iduser){
    return $this->MUser->mGetAllPostByIDUser($iduser);
  }

  public function getPublicPost(){
    echo json_encode($this->Mlogin->mGetViewPublicPostInit());
  }

  public function getPostByID(){
    $idPost = $this->input->post('idPost');
    $this->MUser->mGetPostByIDPost($idPost);
  }

  public function loadMorePost(){
    if(!isset($_GET['from'])) die('check starting point');
    $startFrom = $this->input->get('from');
    echo json_encode($this->Mlogin->mGetViewMorePost($startFrom));
  }

}
?>
