<?php
require_once(APPPATH."controllers/Welcome.php");

require "vendor/autoload.php";
use Endroid\QrCode\QrCode;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;

Class Auth extends Welcome {

  public function __construct(){
    parent::__construct();
    $this->load->library('session');
    $this->load->helper('file');
    $this->load->model('Mlogin');
  }

  public function index(){
    if(isset($this->session->userdata['logged_in'])){
      $this->sethomepage($_SESSION['logged_in']['level']);
    }else{
      $data['sPost'] = $this->Mlogin->mGetViewPublicPost();
      $this->load->view('Beranda', $data);
    }
  }

  public function verify(){
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    if( (isset($username)) && (isset($password)) ){
      $data = array('username'=>$username, 'password'=>$password);
      $hashed = $this->Mlogin->loginMatch($data);
      if(password_verify($data['password'], $hashed)){
        $result = $this->Mlogin->loginData($data['username']);
        $session_data = array(
          'iduser'=>$result[0]->c_iduser,
          'username'=>$result[0]->c_username,
          'level'=>$result[0]->c_level
        );
        $this->session->set_userdata('logged_in', $session_data);
        //$this->catatlogin($_SESSION['logged_in']['iduser']);
        $this->sethomepage($_SESSION['logged_in']['level']);
      }else{
        echo 'failed';
        die();
      }
    }
  }

  private function sethomepage($level){
    switch($level){
      case '1':
        redirect('Admin');
        break;
      case '2':
        redirect('V2');
        break;
      default:
        redirect('');
        break;
    }
  }

  public function logout(){
    $this->session->sess_destroy();
    redirect('');
  }

  private function getRealIPAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
  }

  public function catatlogin($iduser){
  date_default_timezone_set("Asia/Bangkok");
  $xml = simplexml_load_file("geoplugin.net/xml.gp?ip=".$this->getRealIPAddr());
  $geoIP = $xml->geoplugin_request;
  // $geoLatitude = $xml->geoplugin_latitude;
  // $geoLongitude = $xml->geoplugin_longitude;
  $waktu = date('Y-m-d H:i:s');
  $b = $_SERVER['HTTP_USER_AGENT'];
  $log = '"' . $waktu . '", "' . $iduser . '", "' . $geoIP . '", "' . $b . '",';
  $visitorlog = fopen("visitors.txt", "aw+") or die("file error");
  fwrite($visitorlog, $log . "\n");
  fclose($visitorlog);
  }

  public function getPublicPost(){
    echo json_encode($this->Mlogin->mGetViewPublicPostInit());
  }

  public function getPublicPostLatest(){
    echo json_encode($this->Mlogin->mGetViewPublicPostInitLatest());
  }

  public function showPostBySearch(){
    if($this->input->post('idpost') == null){
      die('ERRRRR');
    }else{
      echo json_encode($this->Mlogin->mGetViewPublicPostById($this->input->post('idpost')));
    }
  }

  public function getPostById(){
    if($this->input->post('idpost') == null){
      die('ERRRRR');
    }else{
      echo json_encode($this->Mlogin->mGetPostById($this->input->post('idpost')));
    }
  }

  public function tampung(){
    $data = $_POST;
    $this->load->view('tampung', $data);
  }

  public function loadMorePost(){
    if(!isset($_GET['from'])) die('check starting point');
	if($_GET['from'] < 6) die('low starting point');
    $startFrom = $this->input->get('from');
    echo json_encode($this->Mlogin->mGetViewMorePost($startFrom));
  }

  /*QR Code function */

  public function borongan(){
    $query = $this->db->query('SELECT * FROM qr_borongan');
    $data['karyawans'] = $query->result();
    $this->load->view('others/qrcodes/index', $data);
  }

  public function qrcodes(){
    $this->load->view('others/qrcodes/index');
  }

  public function qrGenerate(){
    $qr = $this->input->post('qrtext');
    if(!isset($qr)){
      die('please input data source');
    } 
    header("Content-Type: image/png");
    $qrcode = new QrCode($qr);
    //echo $qrcode->writeString();
    //die();
    
    $data['qrtext'] = $this->input->post('qrtext');
    $data['qrimage'] = $qrcode->writeString();

    $this->load->view('others/qrcodes/test', $data);   
  }

  public function getGenerated(){
    $qr = $this->input->get('qrtext');
    if(!isset($qr)){
      die('please input data source');
    } 
    header("Content-Type: image/png");
    $qrcode = new QrCode($qr);
    echo $qrcode->writeString();
  }

  public function getGeneratedWithLogo(){
    $qr = $this->input->get('qrtext');
    $imagePath = 'assets/images/logo.png';
    $qrCode = new QrCode($qr);
    $qrCode->setMargin(10);
    $qrCode->setSize(300);
    $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
    $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
    $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
    $qrCode->setLogoPath($imagePath);
    $qrCode->setLogoSize(36, 48);
    $qrCode->setRoundBlockSize(true);
    $qrCode->setValidateResult(false);

    // Send output of the QRCode directly
    header('Content-Type: '.$qrCode->getContentType());
    echo $qrCode->writeString();
  }
  /*End QR Code function */
  
  /* test API Cpanel function */
  public function testApi(){
    //curl -H'Authorization: cpanel username:APITOKEN' 'https://example.com:2083/execute/Module/function?parameter=value'
    
    $curl = curl_init();


    var_dump($curl);
  }


}
?>
