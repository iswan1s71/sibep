<?php

Class MUser extends CI_Model {

  function mGetDataPengguna($iduser){
    $sql = "SELECT * FROM tbl_user WHERE c_iduser='". $iduser ."';";
    $query = $this->db->query($sql);
    return $query->row();
  }

  function mSavePosting($data){
    if($this->doubleTitle($data['postedTitle'])){
      die('exist');
    }
    $sql = "INSERT INTO tbl_postmain (c_idAuthor, c_idGroup, c_postTitle, c_postContent, c_postDate, c_postModifiedDate, c_hasAttachment, c_urlAttachment, c_isPublished)
            VALUES ('"
              . $data['iduser'] . "', '"
              . "group0" . "', '"
              . $data['postedTitle'] . "', '"
              . $data['postedText'] . "', '"
              . $data['postDate'] . "', '"
              . $data['postDate'] . "', '"
              . $data['hasAttachment'] . "', '"
              . $data['urlAttachment'] . "', '"
              . $data['isPublish'] ."');";
    $query = $this->db->query($sql);
    //returning successed posted 
    $strResult = "";
    if ($this->db->affected_rows() >= 1) {
      $qIdPost = ("SELECT c_idPost FROM tbl_postmain WHERE c_postTitle='". $data['postedTitle'] ."';");
      $strQuery = $this->db->query($qIdPost);
      $strResult = $strQuery->row(0)->c_idPost;
    }
    return $strResult;
  }

  function doubleTitle($title){
    $query = $this->db->query("SELECT 'c_postTitle' FROM tbl_postmain WHERE c_postTitle='" . $title . "';");
    if ($query->num_rows() >= 1) {
        return true;
    }
    return false;
  }

  function mUpdatePosting($data){
    $sql = "UPDATE tbl_postmain SET c_postTitle='" . $data['postedTitle'] . "',
         c_postContent='" . $data['postedText'] . "',
         c_postModifiedDate='" . $data['dateModifed'] ."',
         c_isModified=1
         WHERE c_idPost='" . $data['idpost'] . "';";
    $query = $this->db->query($sql);
  }

  function mHidePost($idPost){
    $sql = "UPDATE tbl_postmain SET c_isDeleted=1, c_isPublished=0 WHERE c_idPost='" . $idPost . "';";
    $query = $this->db->query($sql);
  }

  function mRemovePost($idPost){
    $sql = "DELETE FROM tbl_postmain WHERE c_idPost='" . $idPost . "';";
    $query = $this->db->query($sql);
    return $this->db->affected_rows();
  }

  function mHidePostReturn($idPost){
    $sql = "UPDATE tbl_postmain SET c_isDeleted=1, c_isPublished=0 WHERE c_idPost='" . $idPost . "';";
    $query = $this->db->query($sql);
    return $this->db->affected_rows();
  }

  function mGetAllPostByIDUser($iduser){
    $sql = "SELECT * FROM tbl_postmain WHERE c_idAuthor='". $iduser . "' ORDER BY `tbl_postmain`.`c_postModifiedDate` DESC; ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function mGetIndexAllPostByIDUser($iduser){
    $sql = "SELECT c_idPost, c_postTitle FROM tbl_postmain WHERE c_idAuthor='". $iduser . "'; ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function mGetLimitPostByIDUser($iduser, $limitStart){
    $Start = ($limitStart - 1) * 10;
    $sql = "SELECT * FROM tbl_postmain WHERE c_idAuthor='" . $iduser . "' ORDER BY c_postModifiedDate DESC LIMIT ". $Start .", 10;";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function mMakePublish($idPost){
    $sql = "UPDATE tbl_postmain SET c_isPublished=1 WHERE c_idPost='" . $idPost . "';";
    $query = $this->db->query($sql);
  }

  function mMakeDraft($idPost){
    $sql = "UPDATE tbl_postmain SET c_isPublished=0 WHERE c_idPost='" . $idPost . "';";
    $query = $this->db->query($sql);
  }

  function mGetPostByIDPost($id){
    $sql = "SELECT * FROM tbl_postmain WHERE c_idPost=" . $id . ";";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
  function mGetPostByIDPostObj($id){
    $sql = "SELECT * FROM tbl_postmain WHERE c_idPost='" . $id . "';";
    return $this->db->query($sql); 
  }

  function mSingleFieldQuery($id, $field){
    $sql = "SELECT ". $field ." FROM tbl_postmain WHERE c_idPost=" . $id . ";";
    $query = $this->db->query($sql);
    $row = $query->row(0);
    return $row;
  }

  function mGetMaxPost($iduser){
    $sql = "SELECT COUNT(*) as max FROM `tbl_postmain` WHERE c_idAuthor = " . $iduser . ";";
    $query = $this->db->query($sql);
    $row = $query->row(0);
    return $row->max;
  }

}

?>
