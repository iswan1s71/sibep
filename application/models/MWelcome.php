<?php
Class MWelcome extends CI_Model {

  public function loginMatch($data){
    $sql = "SELECT c_password FROM tbl_login WHERE c_username = '" . $data['username'] . "';";
    $hashed = $this->db->query($sql)->row();
    if(!empty($hashed)){
      $hashed = $hashed->c_password;
      return $hashed;
    }else{
      $hashed = "0";
      return $hashed;
    }
  }

  public function loginData($username){
    $condition = "c_username=" . "'" . $username . "'";
    $this->db->select('*');
    $this->db->from('tbl_login');
    $this->db->where($condition);
    $this->db->limit(1);
    $query = $this->db->get();
    if($query->num_rows() == 1){
      return $query->result();
    }else{
      return FALSE;
    }
  }

  public function mGetPublicPost(){
    $sql = "SELECT * FROM tbl_postmain WHERE c_isPublished='1'";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function mGetViewPublicPost(){
    $sql = "SELECT * FROM v_publicpost";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function mGetViewPublicPostById($id){
    $sql = "SELECT * FROM v_publicpost WHERE c_idPost='". $id ."';";
    $query = $this->db->query($sql);
    return $query->row();
  }

  public function mGetPostByID($id){
    $sql = "SELECT * FROM tbl_postmain WHERE c_idPost='" . $id . "';";
    $query = $this->db->query($sql);
    return $query->row();
  }

  public function mGetViewPublicPostInit(){
    $sql = "SELECT * FROM v_publicpost LIMIT 7";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function mGetViewMorePost($startFrom){
    $sql = "SELECT * FROM v_publicpost LIMIT " . $startFrom . ", 6;";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

}
?>
