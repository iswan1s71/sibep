<?php // front page if unauthenticated ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Info LEIBEL</title>
<?php include('common_template/header.php'); ?>
</head>
<body>
<?php
if(!isset($this->session->userdata['logged_in'])){
  require('common_template/navBar.php');
}else{
  require('User/navbar.php');
}
?>
<div class="row mx-auto" id="loadingDiv">
  <h5 class="container text-center">loading please wait... <img src="<?php echo base_url('assets/images/loading.gif'); ?>"></h5>
</div>
<div class="wrapper" style="display:none">
  <section id="contentDepan" class="px-5">
    <div class="row" class="p-3 mx-auto mt-3">
      <?php include('common_template/contentDepan.php'); ?>
    </div>
    <div class="row">
      <div class="container my-5">
        <div class="col-12 text-center"><a href="#" class="font-italic orange" name="morePost">more post</a></div>
      </div>
    </div>
  </section>
</div>
<?php
include('common_template/footer.php');
if(isset($this->session->userdata['logged_in']['iduser'])){
include('User/Modal.php');
}else{
  include('common_template/Modal.php');
}
?>
</body>
</html>

<script>

$(document).ready(function(){
  
  $(this).clickableModal2();

  $('#loadingDiv').show();

  $('#pageFooter').fadeIn(3000);
  var fullText = null;
  $.getJSON('Auth/getPublicPost', function(data){
    var html = '';
    $.each(data, function(key, value){
        html += '<div class="card content p-3" id='+value.c_idPost+'>';
        if(value.c_urlAttachment != ''){
          html += '<img class="card-img-top" src="'+value.c_urlAttachment+'">';
        }
        html += '<div class="card-body -pb mc-hover-white">';
        html += '<h5 class="card-title font-weight-bold">'+value.c_postTitle+'</h5><hr>';
        html += '<div class="card-text" name="shortContent">'+value.c_postContent+'</div>';
        html += '<div class="d-none" name="fullContent">'+value.c_postContent+'</div>';
        html += '<footer class="blockquote-footer pt-3">';
        html += '<span class="text-muted mr-2" title="author">'+value.c_divisi+', </span>';
        html += '<span class="text-muted font-italic" title="author">'+value.c_nama+' </span></br>';
        if(value.c_isModified == 0){
            html += '<cite>posted '+$(this).my_date_format(value.c_postDate)+'</cite>';
        }else{
            html += '<cite>updated '+$(this).my_date_format(value.c_postModifiedDate)+'</cite>';
        }
        html += '</footer></div></div>';
    });
    $('#postBox').html(html);
  }).done(function(){
    $('#loadingDiv').hide();
    $(this).shortingPost();
  });

  $('a[name=morePost]').on('click', function(e){
    e.preventDefault();
    var x = $('#postBox .card.content').length;
    $(this).loadMorePost(x, $('#postBox'));
  });


  $('a[id=search]').on('click', function(){
    $(this).linkActive($(this));
    $.ajax({
      url: 'welcome/searchpage',
      type: 'get',
      success: function(data){
        $('.wrapper').html(data);
      }
    });
  });

  $('a[id=login]').on('click', function(){
    $(this).linkActive($(this));
    $.get('welcome/loginpage', function(data){
      $('.wrapper').html(data);
    });
  });

  $('a[id=download]').on('click', function(){
    $(this).linkActive($(this));
    $.get('welcome/download', function(data){
      $('.wrapper').html(data);
    });
  });

});

(function($){

  $.fn.linkActive = function(elem){
    $('#header2 a').removeClass('active');
    elem.addClass('active');
  }

  $.fn.showPostBySearch = function(idpost){
    var form_data = new FormData();
    form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash();?>');
    form_data.append('idpost', idpost);
    $.ajax({	url: 'Auth/showPostBySearch',
              dataType: 'JSON',
              cache: false,
              contentType: false,
              processData: false,
              data: form_data,
              type: 'post',
              success: function(data){
                $('#modalSearchCardTitle').text(data.c_postTitle);
                $('#modalSearchCardContent').html(data.c_postContent);
                $('#modalSearchCardAuthor').text(data.c_nama);
                var modified = (data.c_isModified == 1)
                  ? $('#modalSearchCardCite').text($(this).my_date_format(data.c_postModifiedDate))
                  : $('#modalSearchCardCite').text($(this).my_date_format(data.c_postDate))
                $('#modalSearchCard').modal('show');
                $('#modalSearchCard').on('hidden.bs.modal', function(){
                  $('#inputDropdownSearch').val('');
                });
              }
      });
  };


}(jQuery));
</script>
