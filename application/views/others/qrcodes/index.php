<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Generate QR Code</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/bootstrap-4.3.1-dist/css/bootstrap.min.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.bootstrap4.min.css');?>"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">


    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/vendor/bootstrap-4.3.1-dist/js/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.bootstrap4.min.js');?>"></script>
    
</head>
<body class="container my-5">
<div class="row mb-5">
    <div class="container text-center">
        <h4 class="text-secodary">Data QR Code karyawan borongan</h4>
    </div>
</div>

<hr class="bg-light">
<div class="row">
    <div class="container-fluid" id="table_borongan_wrapper">
        <table class="table table-hover" id="table_borongan">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Divisi</th>
                    <th scope="col">Posisi</th>
                    <th scope="col">NIK</th>
                    <th scope="col">QR Code</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1;
                    foreach($karyawans as $karyawan){
                        ?>
                        <tr>
                            <td><?= $i++; ?></td>
                            <td name="nama"><?= $karyawan->c_nama; ?></td>
                            <td name="divisi"><?= $karyawan->c_divisi; ?></td>
                            <td name="posisi"><?= $karyawan->c_posisi; ?></td>
                            <td name="nik"><?= $karyawan->c_nik; ?></td>
                            <td>
                            <?php 
                                echo form_open('','class="formqr"');
                                ?>
                                <?php 
                                    $data = array(
                                    'name' => 'qrtext', 'type' => 'hidden', 'value' => $karyawan->c_nik
                                    );
                                    echo form_input($data);
                                ?>
                                </div>
                                <?php
                                $data = array(
                                    'name' => 'button', 'class' => 'btn btn-sm btn-primary', 'value' => 'submit' , 'content' => 'get ID'
                                );
                                echo form_button($data);
                                echo form_close();
                            ?>
                            </td>
                        </tr>
                        <?php
                    }
                ?>    
            </tbody>
        </table>
    </div>
</div>

</body>
<div id="modal_result" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header justify-content-center">
            <h5 class="modal-title" id="exampleModalLabel">- QR Code (Quick Response Code) -</h5>
        </div>
        <div class="modal-body mx-auto">    


            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src=""  id="qrimage">
                <ul class="list-group text-center" id="list_data">
                    <li class="list-group-item"></li>
                    <li class="list-group-item"></li>
                </ul>
            </div>


        </div>
    </div>
  </div>
</div>

</html>
<script>
(function($){

}(jQuery));
$(document).ready(function(){
    $('#table_borongan').dataTable();
    
    $('.formqr').on('submit', function(e){
        e.preventDefault()
    });
    var msg = '1123456789212345678931234567894123456789512345678961234567897123456789812345678991234567891023456789';

    $('tbody').on('click', '.btn-primary', function(e){
            e.preventDefault();
            var boronganId = $(this).siblings('input[name="qrtext"]').val();
            var boronganNama = $(this).parentsUntil('tr').siblings('td[name="nama"]').text();
            var boronganDiv = $(this).parentsUntil('tr').siblings('td[name="divisi"]').text();
            var boronganPos = $(this).parentsUntil('tr').siblings('td[name="posisi"]').text();
            $('#modal_result').on('show.bs.modal', function(){
                $('.modal-body img').attr('src', 'getGeneratedWithLogo?qrtext=' + boronganId);
                $('#list_data > li:nth-child(1)').html(boronganNama);
                $('#list_data > li:nth-child(2)').html(boronganDiv);
            });

            $('#modal_result').modal('show');

    });

});
</script>