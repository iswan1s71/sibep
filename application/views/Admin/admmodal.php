<!-- Modal logout-->
<div id="selesai" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Logout</h4>
      </div>
      <div class="modal-body">
        <p>Selesai dan keluar dari aplikasi ?</p>
      </div>
      	<div class="modal-footer">
          <a href="<?php echo base_url('Auth/logout')?>">
        	<button type="button" class="btn btn-default">Ya, selesai</button></a>
      	</div>
    </div>
  </div>
</div>

<!-- Modal profil -->
<div id="profil" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border-bottom:solid 3px #337ab7;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">admin</h3>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
            <table class="table table-responsive" id="tableprofiladm">
              <thead>
                <tr><p><h4>Data</h4></p></tr>
              </thead>
              <tbody>
                <tr>
                  <td class="jumbotron"><h1>YOU'RE THE OWNER OF THIS SITE</h1></td>
                </tr>
              </tbody>
             </table>
          </div>
      </div>
        <div class="modal-footer">
        </div>
    </div>
  </div>
</div>

<!-- Modal input -->
<div id="modalinputuser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content panel-info">
      <div class="modal-header panel-heading" style="border-bottom:solid 3px #337ab7;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">tambah user baru</h3>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form class="form-group row" id="forminputuser">
            <div class="input-group ui-widget" style="display:!block; margin-bottom:15px;">
              <input type="text" id="add-iduser" placeholder="iduser" class="form-control mb-2" disabled style="opacity: 0.5">
              <input type="text" id="add-name" placeholder="cari nama" class="form-control mb-2" style="width:100%;">
            </div>
            <input type="text" id="add-username" placeholder="username" class="form-control mb-2">
            <input type="text" id="add-password" placeholder="password" class="form-control mb-2">
            <input type="text" id="add-level" placeholder="priviledge as user" class="form-control mb-2" disabled style="opacity: 0.5">
            <hr>
            <button type="button" class="btn btn-outline-primary" id="submitnewuser">submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal delete -->
<div id="modaldeleteuser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content panel-warning">
      <div class="modal-header panel-heading" style="border-bottom:solid 3px #337ab7;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">konfirmasi delete user</h3>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <table class="table table-responsive">
            <tbody>
                <form>
                  <div class="form-group">
                      <div class="row">
                      <h2>HAPUS ?</h2>
                      <button type="button" class="btn btn-warning" id="submitdel">delete</button>
                      </div>
                  </div>
                </form>
            </tbody>
           </table>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>

<!-- Modal update -->
<div id="modalupdateuser" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content panel-success">
      <div class="modal-header panel-heading" style="border-bottom:solid 3px #337ab7;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">update data user</h3>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <form id="formupdateuser"></form>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>
