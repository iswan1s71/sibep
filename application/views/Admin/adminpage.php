<html>
<head>
<title>Admin page</title>
<!doctype html>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/3.3.7/css/bootstrap.css');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/mystyle.css');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.bootstrap.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/4.7.0/css/font-awesome.min.css'); ?>"/>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/3.3.7/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/admin.min.js');?>"></script>
</head>
<?php include 'admmodal.php'; ?>
<body>
<div class="container">
	<div class="row">
		<div class="nav" id="admleftmenu">
			<div class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars fa-2x"></i></a>
				<ul class="dropdown-menu">
					<li class="inner-addon right-addon-2" data-toggle="modal" data-target="#profil">
						<a href="#">profil <i class="glyphicon glyphicon-user"> </i></a>
					</li>
					<li role="separator" class="divider"></li>
					<li class="inner-addon right-addon-2" data-toggle="modal" data-target="#selesai">
						<a href="#">selesai <i class="glyphicon glyphicon-off"> </i></a>
					</li>
				</ul>
				<div class="text-center" id="admtitle"><h4><b>Admin's Page</b></h4></div>
		    </div>

		</div>
	</div>
</div>
<div class="container-fluid">
<div class="row">
	<div class="jumbotron text-center" id="space"><h1>Admin's Page</h1></div>
</div>
	<div class="panel panel-primary" id="l99">
		<div class="panel-heading">
			<div class="panel-title row">
				<div class="col-sm-6 text-left"><h3>Data User</h3></div>
				<div class="col-sm-6 text-right" style="padding-top:15px">
				<i class="fa fa-plus-square-o fa-2x" id="btn-adduser"></i></div>
			</div>
		</div>
		<div class="panel-body">
			<!-- table datauser -->
			<div class="">
				<table class="table table-hover table-responsive table-bordered" id="tbldatauser">
					<thead>
						<tr>
							<th>IDUSER</th>
							<th>IDGROUP</th>
							<th>NAMAUSER</th>
							<th>USERLOGIN</th>
							<th>PASSLOGIN</th>
							<th>LEVEL</th>
							<th>AKSI</th>
						</tr>
					</thead>
					<tbody>
							<!-- php section -->
							<?php 
							//var_dump($datauser);
							foreach ($datauser as $key => $value) {
								$kolom = array_values($value);
								$iduser = $kolom[0];
								$idgroup = $kolom[1];
								$namauser = $kolom[2];
								$userlogin = $kolom[3];
								$passlogin = $kolom[4];
								$level = $kolom[5];
								echo '<tr>
										<td>' . $iduser . '</td>
										<td>' . $idgroup . '</td>
										<td>' . $namauser . '</td>
										<td>' . $userlogin . '</td>
										<td>' . $passlogin . '</td>
										<td>' . $level . '</td>
										<td><i class="fa fa-pencil-square-o lnk-nb" style="padding-right:25%" id="' . $iduser . '"></i>
											<i class="fa fa-times lnk-nb" id="' . $iduser . '"></i>
										</td>
									</tr>';
							}		
								
							?>
							<!-- end php section -->
					</tbody>
				</table>
				<!-- end table datauser -->
			</div>
		</div>
	</div>
</div> <!-- close container-fluid -->
<?php include 'contentbawah.php'; ?>
</body>

</html>