<!--
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lato"/>
-->

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/bootstrap-4.3.1-dist/css/bootstrap.min.css');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/mystyle.css');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/4.7.0/css/font-awesome.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css'); ?>"/>

<script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/vendor/bootstrap-4.3.1-dist/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico"/>

<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/ie/all-ie-only.css');?>"/>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/jquery-1.12.3.js');?>""></script>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/html5shiv.min.js');?>""></script>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/respond.min.js');?>""></script>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/ieauth.js');?>"></script>
<![endif]-->
