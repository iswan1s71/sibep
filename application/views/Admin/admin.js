<script type="text/javascript">
$(document).ready(function() {
  $('#tbldatauser').DataTable();
 	$('.fa-times').each(function(index) {
 		$(this).click(function() {
 		var iduser = $(this).attr('id');
 		$('#modaldeleteuser').modal('toggle');
	   		$('#submitdel').click(function() {
	        var targeturl = "<?php echo base_url('Admin/fDelUser') ;?>";
	        $.ajax({
	            type: 'POST', url: targeturl,
	            data: {
	              'iduser': iduser,
	              '<?php echo $this->security->get_csrf_token_name();?>':'<?php echo $this->security->get_csrf_hash();?>'
	              },
	            dataType: 'text', cache: 'false',
	            success: function(data){
	              $('#modaldeleteuser > div > div > div.modal-body').html(data);
                $('#modaldeleteuser').on('hidden.bs.modal', function () {
                 location.reload(); });
	            }});
		   	});
   		});
   	});

  $('.fa-pencil-square-o').each(function(index) {
    $(this).click(function() {
    var iduser = $(this).attr('id');
    var targeturl = "<?php echo base_url('Admin/finitupdateuser') ;?>";
    $.ajax({
      type: 'POST',
      url: targeturl,
      data: { 'iduser': iduser,
        '<?php echo $this->security->get_csrf_token_name();?>' : '<?php echo $this->security->get_csrf_hash();?>'
      },
      dataType: 'text', cache: 'false',
      success: function(data,xhr){
        $('#formupdateuser').html(data);
        $('#modalupdateuser').modal('toggle');
        }
      });
    });
  });

  $('#btn-adduser').click(function() {
    $('#modalinputuser').modal('toggle');
    $('#submitnewuser').click(function() {
        var i = 0;
        $('.addn').each(function(index) {
          var input = $(this).val();
          if ( input != '' ) { $(this).css({ 'background-color':'transparent'}); i = i+1; return;
          } else { $(this).css({ 'background-color':'#ffffca'}); return; }
        });
        if (i <= 1 ) { return; } else {
          var iduser = $('#add-iduser').val();
          var username = $('#add-username').val();
          var password = $('#add-password').val();
          var level = '2';
          if ((iduser != '') && (username != '') && (password != '') && (level != '')) {
                var targeturl = "<?php echo base_url('Admin/fAddNewUser') ;?>";
                $.ajax({
                    type: 'POST', url: targeturl,
                    data: {
                      'iduser': iduser,
                      'username': username,
                      'password': password,
                      'level': level,
                      '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash();?>'
                      },
                    dataType: 'text', cache: 'false',
                    success: function(data){
                      $('#modalinputuser > div > div > div.modal-body').html(data);
                      $('#modalinputuser').on('hidden.bs.modal', function () {
                       location.reload(); });
                    }});
          } else { alert('periksa kembali isian anda !!!'); return; }}
      });
    });
});
jQuery(function($) {
  function fixDiv() {
    var cache = $('#admleftmenu');
    var title = $('#admtitle');
    var space = $('#space').height() + 48 ;
    if ($(window).scrollTop() > space) {
    	cache.css({'background-color':'#e6e6e6','border-bottom':'2px solid #ddd'});
    	title.css({'display':'inline-block'});
  	}
    else {
    	cache.css({'background-color':'transparent', 'border-bottom':'none'});
    	title.css({'display':'none'});
    }
  }
  $(window).scroll(fixDiv);
  fixDiv();
 });
</script>
