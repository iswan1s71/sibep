<!-- Modal update -->
  <div class="form-group">
      <div class="row">
      <h4>data saat ini : </h4>
        <div class="input-group col-sm-12">
          <div class="col-sm-2">
            <h6 class="cl-prim" id="sym">iduser</h6>
          </div>
          <div class="col-sm-10">
            <input type="text" id="update-iduser" class="form-control input-lg spc-d1 updu"
            placeholder="<?php if (isset($data)) {echo $data['iduser'];} else {echo 'cek';} ;?>" disabled>
          </div>
        </div>
        <div class="input-group col-sm-12">
          <div class="col-sm-2">
            <h6 class="cl-prim" id="sym">username</h6>
          </div>
          <div class="col-sm-10">
              <input type="text" id="update-username" class="form-control input-lg spc-d1 updu"
              placeholder="<?php if (isset($data)) {echo $data['username'];} else {echo 'cek';} ;?>" disabled>
          </div>
        </div>
        <div class="input-group col-sm-12">
          <div class="col-sm-2">
            <h6 class="cl-prim">password</h6>
          </div>
          <div class="col-sm-10">
              <input type="text" id="update-password" class="form-control input-lg spc-d1 updu"
              placeholder="<?php if (isset($data)) {echo $data['password'];} else {echo 'cek';} ;?>">
          </div>
        </div>
        <!-- <div class="input-group col-sm-12">
          <div class="col-sm-2">
            <h6 class="cl-prim">level</h6>
          </div>
          <div class="col-sm-10">
            <input type="text" id="update-level" class="form-control input-lg spc-d1 updu"
            placeholder="<?php if (isset($data)) {echo $data['level'];} else {echo 'cek';} ;?>">
          </div>
        </div> -->

        <hr>
        <button type="button" class="btn btn-primary" id="submitupdate">update</button>
        </div>
  </div>
<script>
$(document).ready(function(){

  var targeturl = "<?php echo base_url('Admin/fupdateuser') ;?>";
  $("#submitupdate").click(function(){
    $.fn.fgetval = function(){
          var v = $(this).val();
          if (v === '') {
            v = $(this).attr('placeholder');
            } else { v = v ;}
            return v;
       };
    var iduser = $('#update-iduser').fgetval();
    var username = $('#update-username').fgetval();
    var password = $('#update-password').fgetval();
    var level = $('#update-level').fgetval();
    //return;
    $.ajax({
      type: 'POST',
      url: targeturl,
      data: {
        'iduser': iduser,
        'username': username,
        'password': password,
        'level': level,
        '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash();?>'
        },
      dataType: 'text', cache: 'false',
      success: function(data){
        $('#formupdateuser').html(data);
          $('#modalupdateuser').on('hidden.bs.modal', function(){
            window.location.href= "<?php echo base_url('Admin'); ?>";
          });
        }
      }); //end ajax
  });
});

</script>
