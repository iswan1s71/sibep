<?php include ('adminHeader.php'); ?>
<?php include ('admmodal.php'); ?>
<?php include ('daftarUser.php'); ?>

<div class="container-fluid text-center bg-light">

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">Admin's Page</a>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item active">
          <a class="nav-link" href="#">Beranda<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('Auth/logout'); ?>">Selesai</a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- <div class="row">
  	<div class="nav" id="admleftmenu">
  		<div class="dropdown">
  			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars fa-2x"></i></a>
  			<ul class="dropdown-menu">
  				<li class="dropdown-item inner-addon right-addon-2" data-toggle="modal" data-target="#profil">
  					<a href="#">profil <i class="glyphicon glyphicon-user"> </i></a>
  				</li>
  				<li role="separator" class="divider"></li>
  				<li class="dropdown-item inner-addon right-addon-2" data-toggle="modal" data-target="#selesai">
  					<a href="#">selesai<i class="glyphicon glyphicon-off"></i></a>
  				</li>
  			</ul>
  			<div class="text-center" id="admtitle"><h4><b>Admin's Page</b></h4></div>
  	  </div>
  	</div>
  </div> -->
</div>

<div class="container-fluid">
	<div class="panel panel-primary p-5" id="l99">
		<div class="panel-heading">
			<div class="panel-title row">
				<div class="col-sm-6 text-left"><h4>Data User</h4></div>
				<div class="col-sm-6 text-right">
				<i class="fa fa-plus-square-o fa-2x" id="btn-adduser"></i></div>
			</div>
		</div>
    <hr/>
		<div class="panel-body">
			<!-- table datauser -->
			<div class="container-fluid">
				<table class="table table-hover table-responsive table-bordered" id="tbldatauser">
					<thead>
						<tr>
							<th class="">IDUSER</th>
							<th class="col">USERNAME</th>
							<!-- <th class="col">LEVEL</th> -->
							<th class="col">AKSI</th>
						</tr>
					</thead>
					<tbody>
							<!-- php section -->
							<?php
							//var_dump($datauser);
							foreach ($datalogin as $key => $value) {
								$kolom = array_values($value);
								$iduser = $kolom[0];
								$username = $kolom[1];
								$password = $kolom[2];
								$level = $kolom[3];
								echo '<tr>
										<td>' . $iduser . '</td>
										<td>' . $username . '</td>
										<td><i class="fa fa-pencil-square-o lnk-nb" style="padding-right:25%" id="' . $iduser . '"></i>
											<i class="fa fa-times lnk-nb" id="' . $iduser . '"></i>
										</td>
									</tr>';
							}
							?>
							<!-- end php section -->
					</tbody>
				</table>
				<!-- end table datauser -->
			</div>
		</div>
	</div>
</div> <!-- close container-fluid -->
<?php include('admin.js'); ?>
