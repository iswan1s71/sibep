<div class="container-fluid bg-light borderb" id="header0">
  <nav class="navbar navbar-light" style="background: unset !important; padding: unset !important;">
    <div class="container">
      <div class="clearfix mx-auto">
        <a class="navbar-brand float-left" href="#">
          <img src="assets/images/logo.png" width="30" height="40" class="d-inline-block align-top" alt="">
        </a>
        <h1 class="text-dark d-inline-block mb-0" id="title-text-lg">Informasi LEIBEL</h1>
        <h1 class=" text-dark mb-0" id="title-text-sm">Info Leibel</h1>
      </div>
    </div>
  </nav>
</div>

<div class="container-fluid py-2" id="header2">
  <ul class="nav justify-content-center">
    <li class="nav-item align-self-center">
      <a class="nav-link text-dark borderb active" href="<?php echo base_url('/'); ?>" id="home">
      <i class="fa fa-home mr-3"></i>Home</a>
    </li>
    <li class="nav-item align-self-center">
      <a class="nav-link text-dark borderb" href="#" id="search">
      <i class="fa fa-search mr-3"></i>Cari</a>
    </li>
    <li class="nav-item align-self-center">
      <a class="nav-link text-dark borderb" href="#" id="login">
      <i class="fa fa-sign-in mr-3"></i>Login</a>
    </li>
    <li class="nav-item align-self-center">
      <a class="nav-link text-dark borderb" href="#" id="download">
      <i class="fa fa-cloud-download mr-3"></i>Download</a>
    </li>
  </ul>
</div>

<div class="overlay" style="z-index:9;"></div>
<script>
$(document).ready(function(){
  $('.dropdown').on('shown.bs.dropdown', function(event){
    $('.overlay').fadeIn('fast');
  });
  $('.dropdown').on('hidden.bs.dropdown', function(event){
    $('.overlay').fadeOut('fast');
  })
});
(function($){
  $.fn.hideOverlay = function(){
    var hide = $('div.dropdown-menu').hasClass('show') ? console.log('show') : console.log('hide');
  };
}(jQuery));
</script>
