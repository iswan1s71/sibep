<section class="px-3" id="downloadBox">
  <div class="row my-5">
    <div class="container mb-5 text-center">
        <div class="list-group list-group-flush">
            <div class="list-group-item d-flex flex-row justify-content-center">
                <div class="px-3">
                    mobile application version.1.0 (android)    
                </div>
                <div class="px-3">
                    <a href="#" class="btn btn-sm btn-primary text-white" name="sibep-apk" type="download">download</a>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function(){

    $('#downloadBox').on('click', 'a', function(e){
        e.preventDefault();
        //create params //
        var datatype = $(this).attr('type');
        var name = $(this).attr('name');
        var form_data = new FormData();
        form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash();?>');
        form_data.append('name', name);
        form_data.append('type', datatype);
        $.ajax({
            url : 'welcome/downloader',
            type : 'post',
            data : form_data,
            cache: false,
            contentType: false,
            processData: false,
            success : function(data){
                $("body").append("<iframe src='" + data + "' style='display: none;' ></iframe>");
            }
        });

    });

});
</script>
