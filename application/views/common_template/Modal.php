<!-- Modal -->
<div class="modal fade" id="modalMainCard" tabindex="-1" role="dialog" aria-labelledby="modalMainCardTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light">
        <span class="lead modal-title" id="modalMainCardTitle">Post Terbaru</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="card-img" src="" alt="" style="height:100%;">
        <div class="text-dark pt-3">
          <h5 class="card-title mx-3">info utama</h5>
          <p class="card-text m-3">isi info utama.</p>
        </div>
      </div>
      <div class="modal-footer bg-light">
        <footer class="text-dark">
          <span class="text-dark" title="author">MIS </span><br/>
          <cite title="Source Title" style="font-size:80%;">(Management Information System)</cite>
        </footer>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalSearchCard" tabindex="-1" role="dialog" aria-labelledby="modalSearchCardTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <img class="card-img" src="" alt="" style="">
        <div class="text-dark pt-3">
          <h5 class="card-title mx-3" id="modalSearchCardTitle">info utama</h5>
          <p class="card-text m-3" id="modalSearchCardContent">isi info utama.</p>
        </div>
      </div>
      <div class="modal-footer bg-light">
        <footer class="text-dark">
          <span class="text-dark" title="author" id="modalSearchCardAuthor">MIS </span><br/>
          <cite title="Source Title" style="font-size:80%;" id="modalSearchCardCite">(Management Information System)</cite>
        </footer>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->
