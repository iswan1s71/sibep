<section class="px-3" id="loginBox">
  <div class="row my-5">
    <div class="container mb-5">
      <?php 
        echo form_open('#', 'class="col-xl-8 col-lg-8 col-md-12 col-sm-12 mx-auto" id="form_register"');
        ?>
        <div class="form-group">
          <label for="employee">Employee name</label>
          <?php 
            $data = array(
              'id' => 'select_user',
              'name' => 'employee', 'class'=> 'form-control', 'placeholder' => 'select your name', 'autocomplete' => 'off',
            );
            echo form_input($data);
          ?>
        </div>

        <div class="row mx-auto" id="responseDiv"></div>

        <div class="form-group">
          <label for="jabatan">Division</label>
          <?php 
            $data = array(
              'name' => 'jabatan', 'class'=> 'form-control', 
            );
            echo form_input($data, '', ' disabled');
          ?>
        </div>

            <hr/>

            <?= form_hidden('iduser', ''); ?>


        <div class="form-group">
            <label for="username">Create username</label>
            <?php 
              $data = array(
                'name' => 'username', 'class' => 'form-control', 'placeholder' => 'username', 'autocomplete' => 'off',
              );
              echo form_input($data, '', ' disabled'); 
            ?>
        </div>

        <div class="form-group">
            <label for="password">Create password</label>
            <?php 
              $data = array(
                'name' => 'password', 'class' => 'form-control', 'placeholder' => 'password', 'autocomplete' => 'off',
              );
              echo form_password($data, '', ' disabled'); 
            ?>
        </div>

        <div class="form-group">
            <label for="password2">Retype password</label>
            <?php 
              $data = array(
                'name' => 'password2', 'class' => 'form-control', 'placeholder' => 'password', 'autocomplete' => 'off',
              );
              echo form_password($data, '', ' disabled'); 
            ?>
        </div>

        <?php
          $data = array(
            'type' => 'submit', 'class' =>'my-3 btn btn-primary', 'value' => 'submit' 
          );
          echo form_button($data, 'register', 'disabled');
          echo form_close();
        ?>  

    </div>
  </div>

  <div class="row" id="responseValidate" style="display: none;"></div>
        
</section>

<style>
.ui-autocomplete {
  z-index: 9999;
}
</style>
<script type="text/javascript">
var msg_cant_create = "<div class='h6 mx-auto'><strong>sorry, account is unavailable at this time.</strong><span class='text-secondary'> Information please contact MIS.</span></div>";
var msg_registered = "<div class='h6 mx-auto'><strong class='text-success'>already registered,</strong><span class='text-secondary'> please login.</span></div>";
var msg_password_min8 = "<div class='h6 mx-auto'><strong class='text-danger'>password must be minimum 8 character & not contain special special character (~! @ # $ % ^ &).</strong></div>";
var msg_password_mismatch = "<div class='h6 mx-auto'><strong class='text-danger'>password confirmation not match.</strong></div>";
var msg_username_min4 = "<div class='h6 mx-auto'><strong class='text-danger'>username must be minimum 4 character & not contain special special character (~! @ # $ % ^ &).</strong></div>";
var msg_username_exist = "<div class='h6 mx-auto'><strong class='text-danger'>username already taken, choose another one</strong></div>";
var msg_registering = "<div class='h6 mx-auto'><strong class='text-success'>registering, please wait</strong></div>";
var msg_register_success ="<div class='h6 mx-auto text-center'><strong class='text-success'>register success, please login</strong></div>";
(function($){

  $.fn.validateRegister = function(){
    var err_status = 0;
      if($("input[name=username]").val().length < 4){
        err_status = 1;
      }
      if($("input[name=password]").val().length < 8){
        err_status = 2
      }
      if($("input[name=password2]").val().length < 8){
        err_status = 2;
      }
      if($("input[name=password]").val() !== $("input[name=password2]").val()){
        err_status = 3;
      }

      switch(err_status){
        case 1: $('#responseValidate').html(msg_username_min4).show();
        break;
        case 2: $('#responseValidate').html(msg_password_min8).show();
        break;
        case 3: $('#responseValidate').html(msg_password_mismatch).show();
        break;
        default: $('#responseValidate').html(msg_registering).show();
        break;
      }
    return err_status;  
  }
  
  $.fn.nextAllow = function(){
    $("input[name=username]").prop("disabled", false);
    $("input[name=password]").prop("disabled", false);
    $("input[name=password2]").prop("disabled", false);
    $(".btn-primary").prop("disabled", false);
  }
  $.fn.nextDisallow = function(){
    $("input[name=username]").prop("disabled", true);
    $("input[name=password]").prop("disabled", true);
    $("input[name=password2]").prop("disabled", true);
    $(".btn-primary").prop("disabled", true);
  }
  
}(jQuery));

$('.btn-primary').on('click', function(e){
  e.preventDefault();
  var err_check = $(this).validateRegister();
  if(err_check){
    return false;
  }
  console.log('registering');
  $('#form_register').submit();
});

$('#form_register').on('submit', function(e){
  e.preventDefault();
  var iduser = $('#form_register').attr('name');
  var username = $("input[name=username]").val();
  var password = $("input[name=password]").val();
  $.ajax({
    url:'Welcome/registerUser',
    type:'POST',
    data: $(this).serialize(),
    success: function(response){
      if(response === 'exist'){
        $('#responseValidate').html(msg_username_exist).show();
      }else{
        if(response === 'success'){
          $('#responseValidate').remove();
          $('#form_register').html(msg_register_success).show();
        }else{
          $('#responseValidate').html(response).show();
        }
      }
    }
  });
});

</script>

<?php
$n = count($datauser['id']);
echo '<script>$(function(){var cache = [';
    for ($i=0; $i < $n; $i++) {
    	$iduser = rtrim($datauser['id'][$i], ' '); $nama = rtrim($datauser['nama'][$i], ' ');
    	echo '{ value:"' . $iduser . '", label:"' . $nama .'" },';
    }
echo '];

$("#select_user").autocomplete({
    	source: cache,
    	focus: function(event, ui) { 
        event.preventDefault(); 	
    		},
    	select: function(event, ui){ 
        event.preventDefault();
        var selected = $(this).val(ui.item.label);
        $(this).prop("disabled", true)
        $("#responseDiv").hide();
        $(this).nextDisallow();
        $.ajax({
          url: "Welcome/fGetDivisionForRegister",
          type: "GET",
          data: { "idkaryawan" : ui.item.value },
          success: function(data){
            var username = data[0].c_username;
            var jabatan = data[0].c_jabatan;
            var status = data[0].c_status;
            if(status === "0"){
              if(jabatan === ""){
                $("input[name=jabatan]").val("");
                $("#responseDiv").html(msg_cant_create).show();
              }else{
                $("input[name=jabatan]").val(data[0].c_jabatan);
                $("input[name=iduser]").val(data[0].c_iduser);
                $(this).nextAllow();
              }
            }else{
              $("input[name=jabatan]").val(data[0].c_jabatan);
              $("#responseDiv").html(msg_registered).show();
            }
          }
        });

    		}
    	});
});
</script>';
?>