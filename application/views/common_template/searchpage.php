<section class="px-3" id="searchBox">
  <div class="row my-5">
    <div class="container mb-5">
      <form action="" class="col-xl-8 col-lg-8 col-md-12 col-sm-12 mx-auto">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="search post" aria-describedby="basic-addon1" id="inputDropdownSearch">
        </div>
      </form>
    </div>
  </div>
</section>


<?php
$n = count($sPost);
echo '<script>
  $(function(){
    var cache = [';
    for ($i=0; $i<$n; $i++){
    	$idPost = $sPost[$i]['c_idPost'];
      $judul  = $sPost[$i]['c_postTitle'];
    	echo '{ value:"' . $idPost . '", label:"' . $judul .'"},';
    }
echo '];

$("#inputDropdownSearch").autocomplete({
    	source: cache,
    	focus: function(event, ui) {
        event.preventDefault();
    		},
    	select: function(event, ui){
        event.preventDefault();
        $(this).val(ui.item.label);
    		$("#inputIdPost").val(ui.item.value);
        $(this).showPostBySearch(ui.item.value);
    		}
    	});
});
</script>';
?>



