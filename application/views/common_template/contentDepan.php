<div class="row" id="PublicPost">
  <div class="container p-5" id="main-card">
    <div class="card content mb-3 p-3 border-primary" style="max-height:271px; overflow:hidden;">
      <img class="card-img" src="" alt="">
      <div class="card-body">
        <h5 class="card-title text-dark">info utama</h5>
        <div class="card-text">isi info utama</div>
        <hr/>
        <footer class="blockquote-footer">
          <span class="" title="author">MIS </span><br/>
          <cite title="Source Title" style="">(Management Information System)</cite>
        </footer>
      </div>
    </div>
  </div>
  <div class="col-xl-5 col-lg-5 col-sm-12 col-md-12 d-none">
    <div class="card p-1 bg-white text-dark border-primary content" id="secondary-card">
      <blockquote class="blockquote mb-0 card-body mx-auto">
        <?php
        $masuk = isset($this->session->userdata['logged_in']) && $this->session->userdata['logged_in'] != null;
        ?>
        <p>
        <?php
        $pesan = $masuk ? "<small>Informasi LEIBEL</small>" : "Harap login untuk menulis";
        echo $pesan;
        ?>
        </p>
        <footer class="blockquote-footer text-primary bg-light">
          <small class="text-primary">
            MIS <cite title="Source Title">(Management Information System)</cite>
          </small>
        </footer>
      </blockquote>
    </div>
  </div>
  <div class="container px-5">
    <div class="card-columns" id="postBox"></div>
  </div>
</div>
