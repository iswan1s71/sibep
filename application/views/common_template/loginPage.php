<section class="px-3" id="loginBox">
  <div class="row my-5">
    <div class="container">
      <?php 
        echo form_open('Auth/verify', 'class="col-xl-8 col-lg-8 col-md-12 col-sm-12 mx-auto" id="form_login"');
        $name = 'name';
        $class = 'class';
        $ph = 'placeholder';
        $ac = 'autocomplete';
        ?>
        <div class="form-group">
          <label for="username">Username</label>
          <?php 
            $data = array(
              $name => 'username', $class=> 'form-control', $ph => 'enter username', $ac => 'off',
            );
            echo form_input($data);
          ?>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <?php 
              $data = array(
                $name => 'password', $class=> 'form-control', $ph => 'password', $ac => 'off',
              );
              echo form_password($data); 
            ?>
        </div>

        <div class="form-group form-check">
          <input type="checkbox" class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Remember this device</label>
        </div>

        <?php
          $data = array(
            $name => 'submit', $class=>'btn btn-primary', 'value' => 'submit' 
          );
          echo form_submit($data);
          echo form_close();
        ?>  

    </div>
  </div>

  <div class="row mx-auto" id="responseDiv"></div>

  <div class="row mt-5 mb-3">
    <div class="container text-center">
          <h6 class=""><span class="text-secondary">dont have an account ? </span><a href="#" class="" id="register"> Register </a></h6>
    </div>
  </div>

        

</section>

<script>
$(document).ready(function(){
  var message_failed = '<div class="h6 text-center" id="message_failed"><strong class="text-danger">login failed. </strong><span class="text-secondary">Please check username and password</span></div>';
  var message_success = '<div class="h6 text-center" id="message_failed"><strong class="text-success">login successful. </strong><span class="text-secondary">refreshing page, please wait</span></div>';
  $('#form_login').submit(function(e){
    e.preventDefault();
    var valid = $(this).validateLogin();
    if(!valid){
      return false;
    }
    
    $.ajax({
      url:'Auth/verify',
      data: $(this).serialize(),
      dataType: 'text',
      type: 'POST',
      beforeSend: function() {
        $('#responseDiv').html('<h6 class="container text-center">loading please wait... <img src="<?php echo base_url('assets/images/loading.gif'); ?>"></h6>').fadeIn('fast');
      },
      success: function(response){
        if(response === 'failed'){
          $('#responseDiv').html(message_failed).show();
        }else{ 
          $('#responseDiv').html(message_success).show();
          location.reload(); 
        }
      }
    });
  });

  $('a[id=register]').on('click', function(){
    $(this).linkActive($(this));
    $.ajax({
      url: 'welcome/register',
      type: 'get',
      success: function(data){
        $('.wrapper').html(data);
      }
    });
  });

});
</script>