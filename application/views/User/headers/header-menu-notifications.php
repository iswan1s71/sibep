<li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>
    <ul aria-labelledby="notifications" class="dropdown-menu">
    <li><a rel="nofollow" href="#" class="dropdown-item"> 
        <div class="notification d-flex justify-content-between">
            <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>
            <div class="notification-time"><small>4 minutes ago</small></div>
        </div></a></li>
    <li><a rel="nofollow" href="#" class="dropdown-item"> 
        <div class="notification d-flex justify-content-between">
            <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
            <div class="notification-time"><small>4 minutes ago</small></div>
        </div></a></li>
    <li><a rel="nofollow" href="#" class="dropdown-item"> 
        <div class="notification d-flex justify-content-between">
            <div class="notification-content"><i class="fa fa-upload"></i>Server Rebooted</div>
            <div class="notification-time"><small>4 minutes ago</small></div>
        </div></a></li>
    <li><a rel="nofollow" href="#" class="dropdown-item"> 
        <div class="notification d-flex justify-content-between">
            <div class="notification-content"><i class="fa fa-twitter"></i>You have 2 followers</div>
            <div class="notification-time"><small>10 minutes ago</small></div>
        </div></a></li>
    <li><a rel="nofollow" href="#" class="dropdown-item all-notifications text-center"> <strong> <i class="fa fa-bell"></i>view all notifications                                            </strong></a></li>
    </ul>
</li>