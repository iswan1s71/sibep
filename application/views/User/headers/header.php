<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Beton Elemen Persada</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/bootstrap-4.3.1-dist/css/bootstrap.min.css');?>"/>
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/4.7.0/css/font-awesome.min.css'); ?>"/>
    <!-- Fontastic Custom icon font-->
    <!-- <link rel="stylesheet" href="assets/distribution/css/fontastic.css"> -->
    <!-- Google fonts - Roboto -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"> -->

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">

    <!-- jQuery Circle-->
    <!-- <link rel="stylesheet" href="assets/distribution/css/grasp_mobile_progress_circle-1.0.0.min.css"> -->
    <!-- Custom Scrollbar-->
    <!-- <link rel="stylesheet" href="assets/distribution/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css"> -->
    <!-- theme stylesheet-->
    <!-- <link rel="stylesheet" href="assets/distribution/css/style.default.css" id="theme-stylesheet"> -->
    <!-- Custom stylesheet - for your changes-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/welcome.css');?>"/>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/dataTables.min.css'); ?>"/> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css'); ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/distribution/summernote/summernote-bs4.css'); ?>"/>
    <!-- Favicon-->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- JavaScript files-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
    <!--[if lt IE 9]>
      <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/ie/all-ie-only.css');?>"/>
      <script type="text/javascript" src="<?php echo base_url('assets/ie/jquery-1.12.3.js');?>""></script>
      <script type="text/javascript" src="<?php echo base_url('assets/ie/html5shiv.min.js');?>""></script>
      <script type="text/javascript" src="<?php echo base_url('assets/ie/respond.min.js');?>""></script>
      <script type="text/javascript" src="<?php echo base_url('assets/ie/ieauth.js');?>"></script>
    <![endif]-->
    <!-- unused 
    <script src="assets/distribution/vendor/chart.js/Chart.min.js"></script>
    <script src="assets/distribution/js/charts-home.js"></script>
    -->
    <!-- Main File-->
    <!-- <script src="assets/distribution/vendor/jquery-validation/jquery.validate.min.js"></script> -->
    <!-- <script src="assets/distribution/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="assets/distribution/js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="assets/distribution/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="assets/distribution/js/front.js"></script> -->
    <script src="assets/distribution/summernote/summernote-bs4.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.min.js');?>"></script> -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/ellipsis.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url('assets/js/pagination.min.js');?>"></script> -->
    <script type="text/javascript" src="<?php echo base_url('assets/vendor/bootstrap-4.3.1-dist/js/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/default.js'); ?>"></script>

    <!-- firebase core script -->
    <script src="https://www.gstatic.com/firebasejs/6.1.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/6.1.0/firebase-database.js"></script>
    <script>
    
    // config sibep-notification
    // var firebaseConfig = {
    //   apiKey: "AIzaSyCnYZ_jsMGKouq1KTGFQhGJkr9PZdQ-EwE",
    //   authDomain: "sibep-notification.firebaseapp.com",
    //   databaseURL: "https://sibep-notification.firebaseio.com",
    //   projectId: "sibep-notification",
    //   storageBucket: "sibep-notification.appspot.com",
    //   messagingSenderId: "119773368647",
    //   appId: "1:119773368647:web:ea613781cee10759"
    // };

    // config hello firebase
    var firebaseConfig = {
      apiKey: "AIzaSyDuQ2vzFemq_6pqpedSjxoUzST4Mn99z18",
      authDomain: "hello-firebase-6bd12.firebaseapp.com",
      databaseURL: "https://hello-firebase-6bd12.firebaseio.com",
      projectId: "hello-firebase-6bd12",
      storageBucket: "hello-firebase-6bd12.appspot.com",
      messagingSenderId: "354690179662",
      appId: "1:354690179662:web:cbfcbed861f73722"
    };

    firebase.initializeApp(firebaseConfig);
    </script>
    
  </head>