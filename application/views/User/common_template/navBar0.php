<div class="container-fluid bg-light pt-3 pb-1 borderb fixed-top">
  <nav class="navbar navbar-light">
    <div class="container">
      <div class="clearfix">
        <a class="navbar-brand float-left" href="#">
          <img src="assets/images/logo.png" width="30" height="40" class="d-inline-block align-top" alt="">
        </a>
        <span class="display-8 text-dark d-inline-block mb-0 mt-3" id="title-text-lg">Informasi LEIBEL</span>
        <span class="display-8 text-dark mb-0 mt-3" id="title-text-sm">Info Leibel</span>
      </div>

      <div class="container float-right" style="position:absolute; right:3vw;" id="dropdownToggler">
        <div class="">
          <a class="float-right text-dark orange mx-3" href="#" id="dropdownMenuLogin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-sign-in"></i><span class=""> Login</span>
          </a>
          <div class="dropdown-menu dropdown-menu-right -borderrad mt-4" aria-labelledBy="dropdownMenuLogin" style="border-bottom:none !important; padding-bottom:unset !important;">
              <?php
              echo form_open('Auth/verify', 'class="bs px-3" id="formlogin" style="min-width:220px"');
              $rand=rand();
              $_SESSION['rand']=$rand;
              $data = array( 'type' => 'hidden', 'name' => 'randcheck', 'value' => $rand );
              echo form_input($data);
              $cls 	= 'form-control form-control-lg ph2 -borderrad my-2';
              $clsb = 'btn btn-warning text-light text-center my-3 mx-3 -borderrad';
              $i = 'id';
              $n = 'name';
              $c = 'class';
              $ph = 'placeholder';
              $ac = 'autocomplete';
              $data = array( $i =>'username', $n =>'username', $ph =>'username', $c => $cls, $ac => 'off');
              echo form_input($data);
              $data = array( $i =>'password', $n =>'password', $ph =>'password', $c => $cls);
              echo form_password($data);
              $data = array( 'value' =>'login', $n =>'submit', $c => $clsb, $i =>'btnlogin');
              echo form_submit($data);
              echo form_close();
              ?>
          </div>
        </div>
        
        <div class="">
          <a class="float-right text-dark orange mx-3" href="#" id="dropdownSearch" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-search"></i><span> Cari</span>
          </a>
          <div class="bs dropdown-menu dropdown-menu-right -borderrad mt-4 py-3" aria-labelledby="dropdownSearch" style="border-bottom:none !important; width:50%" id="boxDropdownSearch" role="menu">
            <input type="text" class="form-control form-control-lg ph2 -borderrad my-2" id="inputDropdownSearch" placeholder="cari" style="width:100%">
            <form action="#" target="_self" id="">
              <input type="text" class="d-none" name="inputIdPost" id="inputIdPost" value="">
            </form>
          </div>
        </div>

      </div>

    </div>
  </nav>
</div>
<div class="overlay" style="z-index:9;"></div>
<?php include('navBar0Search.php'); ?>
<script>
$(document).ready(function(){
  $('.dropdown').on('shown.bs.dropdown', function(event){
    $('.overlay').fadeIn('fast');
  });
  $('.dropdown').on('hidden.bs.dropdown', function(event){
    $('.overlay').fadeOut('fast');
  })
});
(function($){
  $.fn.hideOverlay = function(){
    var hide = $('div.dropdown-menu').hasClass('show') ? console.log('show') : console.log('hide');
  };
}(jQuery));
</script>
