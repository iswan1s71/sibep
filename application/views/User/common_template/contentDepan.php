<div class="row" id="PublicPost">
  <div class="col-12">
    <div class="card content mb-3" id="main-card" style="max-height:171px; overflow:hidden;">
      <img class="card-img" src="" alt="">
      <div class="card-img-overlay mc-hover">
        <h5 class="card-title text-dark">info utama</h5>
        <p class="card-text">isi info utama.</p>
        <hr class="bg-light"/>
        <footer class="text-warning blockquote-footer">
          <span class="text-warning" title="author">MIS </span><br/>
          <cite title="Source Title">(Management Information System)</cite>
        </footer>
      </div>
    </div>
  </div>

  <div class="col-12">
    <div class="card-columns" id="postBox"></div>
  </div>
</div>
