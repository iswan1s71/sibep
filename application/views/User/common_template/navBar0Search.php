<?php
$n = count($sPost);
echo '<script>
  $(function(){
    var cache = [';
    for ($i=0; $i<$n; $i++){
    	$idPost = $sPost[$i]['c_idPost'];
      $judul  = $sPost[$i]['c_postTitle'];
    	echo '{ value:"' . $idPost . '", label:"' . $judul .'"},';
    }
echo '];

$("#inputDropdownSearch").autocomplete({
    	source: cache,
    	focus: function(event, ui) {
        event.preventDefault();
    		},
    	select: function(event, ui){
        event.preventDefault();
        $(this).val(ui.item.label);
    		$("#inputIdPost").val(ui.item.value);
        $(this).showPostBySearch(ui.item.value);
    		}
    	});
});
</script>';
?>
