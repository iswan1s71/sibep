<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-4.1.3-dist/css/bootstrap.min.css');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/4.7.0/css/font-awesome.min.css'); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/mystyle.css');?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery-ui.css'); ?>"/>

<script type="text/javascript" src="<?php echo base_url('assets/js/popper.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap-4.1.3-dist/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ellipsis.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.min.js');?>"></script>

<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/ie/all-ie-only.css');?>"/>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/jquery-1.12.3.js');?>""></script>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/html5shiv.min.js');?>""></script>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/respond.min.js');?>""></script>
	<script type="text/javascript" src="<?php echo base_url('assets/ie/ieauth.js');?>"></script>
<![endif]-->

<link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url('assets/images/favicon.ico.png'); ?>'/>
