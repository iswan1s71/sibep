<div class="container-fluid bg-light borderb" id="header1">
  <nav class="navbar navbar-light" style="background: unset !important; padding: unset !important;">
    <div class="container d-flex justify-content-between">

      <div class="p-2 mr-3 navbar navbar-light bg-light">
        <button class="btn btn-light navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" id="btn-navbarToggleExternalContent">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>

      <div class="p-2">
        <a class="navbar-brand float-left" href="#">
          <img src="assets/images/logo.png" width="30" height="40" class="d-inline-block align-top" alt="">
        </a>
        <h1 class="text-dark d-inline-block mb-0 small-h1" id="title-text-lg">Informasi LEIBEL</h1>
        <h1 class="text-dark mb-0 small-h1" id="title-text-sm">Info Leibel</h1>
      </div>

      <div class="p-2 dropdown">
        <button class="btn btn-secondary rads" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-user"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
          <span class="dropdown-item disabled"><?php echo $akun->c_nama; ?></span>
          
          <a class="dropdown-item" href="#" name="profile">Profile</a>
          <a class="dropdown-item" href="<?php echo base_url('Auth/logout');?>">Logout</a>
        </div>
      </div>

    </div>

</div>
<script type="text/javascript">
  $('a[name="profile"]').on('click', function(e){
    e.preventDefault();
    console.log('profile');
    $.ajax({
      url:'V2/profile',
      type:'get',
      success: function(data){
        $('#changeable').html(data);
      }
    });
  });
</script>