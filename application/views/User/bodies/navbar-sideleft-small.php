<div class="side-navbar-wrapper bg-light" id="navbarToggleExternalContentSmall" aria-labelledby="btn-navbarToggleExternalContent">
  <!-- Sidebar Navigation Menus small-->
  <div id="left-menu-small">
    <nav class="nav flex-column">
      <a class="nav-link active" href="<?php echo base_url('V2'); ?>"><i class="fa fa-home"></i></a>
      <a class="nav-link" href="#" data-toggle="" data-target="#" id="linkPostDropdownSmall"><i class="fa fa-envelope-o"></i></a>
    </nav>
  </div>

</div>