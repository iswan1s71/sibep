<div class="container" id="changeable">

  <div class="col-11 mx-auto pt-5">
      <h5 class="orange borderb">Global posts</h5>
  </div>

  <div id="contentDepan" class="mx-auto mt-5" style="padding: 0 3rem;">
    <!-- content depan -->
      <div class="col-12">
        <div class="card-deck wrapper" id="postBox"></div>
      </div>
    </div>
    <!-- end content depan -->
  </div>

  <div class="container-fluid mt-3" id="pageFooterUser" style="display:none;">
    <div class="row bg-orange">
      <div class="col p-3">
        <footer class="text-dark text-center">
          <span class="text-dark" title="author">Web App Info LEIBEL - Copyright Management Information System - PT. Beton Elemen Persada </span>
        </footer>
      </div>
    </div>
  </div>
  
</div>



