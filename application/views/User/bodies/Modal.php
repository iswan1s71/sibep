<!-- Modal post-->
<div class="modal fade" id="modalConfirmSubmitPost" tabindex="-1" role="dialog" 
  aria-labelledby="modalLabelConfirmSubmitPost" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabelConfirmSubmitPost">Submit Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      are you sure want to do this ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success btn-submit d-none" title="draft" id="btnConfirmSubmitDraft">Draft</button>
        <button type="button" class="btn btn-primary btn-submit" title="publish" id="btnConfirmSubmitPost">Publish</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal update-->
<div class="modal fade" id="modalConfirmUpdatePost" tabindex="-1" role="dialog" 
  aria-labelledby="modalLabelConfirmSubmitPost" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabelConfirmUpdatePost">Update Post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        are you sure want to do this ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-success btn-submit d-none" title="draft" id="btnConfirmSubmitDraft">Draft</button>
        <button type="button" class="btn btn-primary btn-submit" title="publish" id="btnConfirmUpdatePost">Publish</button>
      </div>
    </div>
  </div>
</div>
<!-- modal hapus post -->
<div class="modal fade" id="modalConfirmRemovePost" tabindex="-1" role="dialog" aria-labelledby="postRemover" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="postRemover">delete post</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        are you sure want to delete post <span class="font-weight-bold" id="titlePost"></span> ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnConfirmRemovePost">Remove</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal hapus post-->

<!-- modal add image -->
<div class="modal fade" id="modalAddImage" tabindex="-1" role="dialog" aria-labelledby="modalLabelAddImage" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabelAddImage">Add or change picture</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img id="modalImagePreview" src="#" class="" style="width:100%"><span class="text-muted">ekstensi .gif / .jpg / .png, max 1Mb : </span>
        <input type="file" class="form-control-file" id="modalImagePicker" class="FileUpload" accept="image/*">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCancelImagePreview">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnSubmitImagePreview">Attach</button>
      </div>
    </div>
  </div>
</div>

<!-- modal publish draft -->
<div class="modal fade" id="modalPublishDraft" tabindex="-1" role="dialog" aria-labelledby="modalPublishDraftTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="card">
          <div class="card-header" id="modalPublishDraftTitle">
            <span>This post is publihed</span>
          </div>
          <div class="card-body">
              <!-- button file -->
              <button type="button" class="btn btn-primary" id="" disabled>Publish</button>
              <button type="button" class="btn btn-success" id="">Unpublish and save to draft</button>
              <!-- end button file-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal publish draft -->

<!-- modal draft published -->
<div class="modal fade" id="modalDraftPublished" tabindex="-1" role="dialog" aria-labelledby="modalDraftPublishedTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="card">
          <div class="card-header" id="modalDraftPublishedTitle">
            <span>This post is not published</span>
          </div>
          <div class="card-body">
              <!-- button file -->
              <button type="button" class="btn btn-primary" id="">Publish</button>
              <button type="button" class="btn btn-success" id="" disabled>Unpublish and save to draft</button>
              <!-- end button file-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end modal draft published -->

<!-- Modal -->
<div class="modal fade" id="modalMainCard" tabindex="-1" role="dialog" aria-labelledby="modalMainCardTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light">
        <span class="lead modal-title" id="modalMainCardTitle">Post Terbaru</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img class="card-img" src="" alt="" style="height:100%;">
        <div class="text-dark pt-3">
          <h5 class="card-title mx-3">info utama</h5>
          <p class="card-text m-3">isi info utama.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalSearchCard" tabindex="-1" role="dialog" aria-labelledby="modalSearchCardTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <img class="card-img" src="" alt="" style="">
        <div class="text-dark pt-3">
          <h5 class="card-title mx-3" id="modalSearchCardTitle">info utama</h5>
          <p class="card-text m-3" id="modalSearchCardContent">isi info utama.</p>
        </div>
      </div>
      <div class="modal-footer bg-light">
        <footer class="text-dark">
          <span class="text-dark" title="author" id="modalSearchCardAuthor">MIS </span><br/>
          <cite title="Source Title" style="font-size:80%;" id="modalSearchCardCite">(Management Information System)</cite>
        </footer>
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

<!-- modal response posting -->
<div class="modal modal-lg fade" id="modalResponsePostSuccess" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header norad">
          <h5 class="modal-title text-success" id="">Success</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>
  </div>
</div>
<div class="modal fade" id="modalResponsePostFail" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-warning" id="">Failed to post data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"></div>
      </div>
  </div>
</div>
<!-- modal response posting -->

