    <footer class="main-footer">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <p>PT. Beton Elemen Persada &copy; 2019</p>
          </div>
          <div class="col-sm-6 text-right d-none">
            <p>Design by <a href="https://bootstrapious.com/p/bootstrap-4-dashboard" class="external">Bootstrapious</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions and it helps me to run Bootstrapious. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </footer>

<script type="text/javascript" src="<?php echo base_url('assets/js/default.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(){

  /* pagination prev */
  $('#prev').on('click', function(){
    if($(this).hasClass('disabled')) return;
    else{
      var l = $('#pagination li.selector.active');
      l.removeClass('active').prev().addClass('active');
      $(this).pagination($('.midResp .card.content'), parseInt(l.text())-1);
    }
  });

  /* pagination next */
  $('#next').on('click', function(){
    if($(this).hasClass('disabled')) return;
    else{
      var n = $('#pagination li.selector.active');
      if(n.text() == $('.selector').length) { $('#next').addClass('disabled'); }
      n.removeClass('active').next().addClass('active');
      n.next().find('a').trigger('click');
      return false;
    }
  });

  /* create pages by ten post */
  var $row = $('#cAllPost .card.content');

  //create fist page wrapper
  $page = 1;
  $split = 10;
  $('#cAllPost .midResp').append('<div class=pages id=page'+$page+'></div>');

  for($i=1; $i<=$row.length; $i++){

    //moving element
    if($i <= $split){
      $row.eq($i-1).appendTo('#page'+$page);
    }
    
    //create new page;
    if($i % 10 == 0){
      $page++;
      $split+=10;
      $('#cAllPost .midResp').append('<div class=pages id=page'+$page+'></div>');
    }
  }
  
  /* init first page of post */

  $('.pages').each(function(){
    var hidden = $(this).attr('id') == 'page1' ? $(this).css('display', 'block') : $(this).css('display', 'none');
  });

  /* pagination pages selector */
  $('.selector a').each(function(){
    $(this).on('click', function(e){
      var n = $(this).text();
      hidingPagesExcept(n);
      e.preventDefault();
    });
  });

  function hidingPagesExcept(n){
    $('.pages').each(function(){
      $(this).css('display', 'none');
      if( $(this).attr('id') == 'page' + n ) $(this).fadeIn('fast');
    });
  }

  /* pages for editing */

  $("#bodyPost").keyup(function(e){
    var bLen = $(this).val();
    if((bLen.length > 10) && (bLen.length != "")){
      $("#buttonPost").prop('disabled', false);
    }else{
      $("#buttonPost").prop('disabled', true);
    }
  });

  $('#navbarLeft').children().find('li').each(function(index){
    $(this).on('click', function(){
      $(this).siblings().removeClass('activated');
      $(this).addClass('activated');
    });
  });

  $(this).fCountAllPost();
  $(this).fCountPublishedPost();
  $(this).fCountDraft();

  
  $('#postedTitle').titleChange();

});


</script>
