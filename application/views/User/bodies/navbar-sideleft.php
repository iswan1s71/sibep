<div class="side-navbar-wrapper bg-light collapse" id="navbarToggleExternalContent" aria-labelledby="btn-navbarToggleExternalContent">
  <!-- Sidebar Navigation Menus-->
  <div id="left-menu">
    <nav class="nav flex-column">
      <a class="nav-link d-flex flex-row active" href="<?php echo base_url('V2'); ?>"><i class="fa fa-home mr-1"></i> Home</a>
      <a class="nav-link d-flex flex-row" href="#leftMenuDropdown" data-toggle="collapse" data-target="#leftMenuDropdown" id="linkPostDropdown"><i class="fa fa-envelope-o mr-1"></i> My posts<i id="linkPostDropdownArrow" class="fa fa-chevron-left"></i></a>      
        <ul class="collapse list-unstyled" id="leftMenuDropdown" aria-labelledby="linkPostDropdown">
          <li class="d-flex flex-row"><a href="#" id="aAllPost">All </a><span class="badge badge-success"><?= $maxPost; ?></span></li>
          <li class="d-flex flex-row"><a href="#" id="aNewPost">New </a><span class="badge badge-info">+</span></li>
          <li class="d-none"><a href="#" id="aDraftPost">Draft </a></li>
          <li class="d-none"><a href="#" id="aPublishedPost">Published </a></li>
        </ul>
    </nav>
  </div>

</div>

<script type="text/javascript">
$(document).ready(function(){
  
  /* dropdown function */
  $('#aAllPost').on('click', function(){
    $.ajax({
      url:'V2/loadUserPostPage',
      type: 'get',
      success: function(data){
        $('#changeable').html(data);
      }
    }).done(function(){
      $(this).instantiateTooltips();
    });
  });

  $('#aNewPost').on('click', function(){
    $.ajax({
      url:'V2/initEditingPost',
      type: 'get',
      success: function(data){
        $('#changeable').html(data);
      }
    }).done(function(){
      $('#summernote').summernote({
        dialogsInBody: true,
        toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['fontnames', ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['height', ['height']],
          ['insert', ['link', 'picture', 'hr']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['misc', ['undo', 'redo','fullscreen']],
        ],
        placeholder: 'write something...',
        height: 300,
        // callbacks: {
        //   onImageUpload: function(data){
        //     data.pop();
        //   },
        //   onPaste: function(e){
        //     e.preventDefault();
        //     return false;
        //   }
        // }
      });
      /* disable right click inside summernote */

    });
  });

  var aLink = $('#leftMenuDropdown li > a');

  aLink.each(function(){
    $(this).on('click', function(){
      clearLink();
      $('#linkPostDropdown').addClass('active');
    });
  });

  function clearLink(){
    $('#left-menu > nav > a').each(function(){
      $(this).removeClass('active');
    });
  }

  $('#leftMenuDropdown').on('show.bs.collapse', function(){
    $('#linkPostDropdownArrow').removeClass('fa-chevron-left').addClass('fa-chevron-down');
  });

  $('#leftMenuDropdown').on('hide.bs.collapse', function(){
    $('#linkPostDropdownArrow').removeClass('fa-chevron-down').addClass('fa-chevron-left');
  });

});
</script>