<div class="container my-5">
    <div class="col-lg-10 col-xl-10 col-md-12 col-sm-12 mx-auto">
        <div class="card">
            <h5 class="card-header">Profile</h5>
            <div class="card-body">
                <div id="profile_picture" class="text-center mb-4 d-none">
                    <h5 class="card-titler">Picture</h5>
                    <div class="img-thumbnail mx-auto my-3">
                    <img src="" alt="" class=""></div>
                </div>
                <div id="profile_identity" class="">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex flex-row">
                            <div class="col ml-5"><strong class="">Name</strong></div><div class="col ml-5 text-secondary"><?= $user->c_nama; ?></div>
                        </li>
                        <li class="list-group-item d-flex flex-row">
                            <div class="col ml-5"><strong class="">Division</strong></div><div class="col ml-5 text-secondary"><?= $user->c_jabatan; ?></div>
                        </li>
                        <!-- <li class="list-group-item d-flex flex-row">
                            <div class="col ml-5"><strong class="">Whatsapp number</strong></div><div class="col ml-5 text-secondary">TEST</div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>  

<script>
$(document).ready(function(){
    $('#profile_picture').on('click', function(){
        $('#modalAddImage').modal('show');
    });
});
</script>