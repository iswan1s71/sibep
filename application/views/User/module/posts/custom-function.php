<?php 
//
// custom function
//

 function lapsetime($publishedtime){
    $now = new DateTime("now");
    $tgt = new DateTime($publishedtime);
    //$interval = date_diff(strtotime('Y-m-d h:m:s', $lapsetime), $now);
    $interval = date_diff($now, $tgt)->format('%R%a');
    $value = abs(intval($interval));
    switch ($value){
        case ($value <= 1) :
        echo "today";
        break;

        case ($value > 7 && $value < 30) :
        echo "about a weeks ago";
        break;

        case ($value > 30) :
        echo "about a months ago";
        break;

        default:
        echo $value . " days ago";
        break;
    }
}

function countPublishedPost($posts){
    if(!isset($posts)) { 
        return 0; 
    }else{
        $j=0;
        for($i=0; $i<count($posts); $i++){
            if($posts[$i]['c_isPublished'] == 1) $j++;
        }
        if($j > 3) return '3+';
        else return $j;
    }  
}

function countDraft($posts){
    if(!isset($posts)) {
        return 0;
    }else{
        $j=0;
        for($i=0; $i<count($posts); $i++){
            if($posts[$i]['c_isDeleted'] == 0 && $posts[$i]['c_isPublished'] == 0) $j++;
        }
        if( $j > 3) return '3+';
        else return $j;
    } 
}

?>