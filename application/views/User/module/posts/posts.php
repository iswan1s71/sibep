<div class="container" style="margin-top:55px;">
  <!-- section highlights -->
  <?php ///include('highlightPost.php'); ?>

  <!-- section all post -->
  <div class="" id="cAllPost" style="padding: 0 3rem;">
    <div class="midResp">
      <?php
        if(!isset($maxPost) || $maxPost <= 0){
            require_once('userNoPost.php');
        }else{
            require_once('userPostData.php');
        }
      ?>
    </div>
    <div class="midEditor" style="display:none;">
      <div class="container"></div>    
    </div>
  </div>
<!-- end section all post -->
</div>


<script type="text/javascript">

/* firebase section */
$.fn.moveToDeletedNodes = function(idpost){
  if(idpost == '') return false;
  
  const dbReff = firebase.database().ref("feeds");
  const dbArchieve = firebase.database().ref("archieves");

  dbReff.orderByChild("idPost").equalTo(idpost).once("value", function(snapshot) {
    snapshot.forEach(function(data){
      F_POSTKEY = data.key;
      if(F_POSTKEY != null || F_POSTKEY != ''){ // console.log("data key : " + F_POSTKEY);
        var nodes = dbReff.child(F_POSTKEY);
        dbArchieve.set(nodes);
      }
    });
  })
}
/* end firebase section */

$(document).ready(function(){
  $('button[name=deletepost]').on('click', function(){

  });
});
</script>