<!-- section New post -->
<div class="container my-5" id="cNewPost">
    <div class="row">
        <form method="post" action="#" class="col-12" id="form-new-post">
            <div class="lead my-3">
            <input type="text" class="form-control" id="postedTitle" placeholder="title cannot be empty" value=<?php echo '"'.$object->c_postTitle.'"'; ?> autocomplete="off">
            </div>
            <div class="" id="summernote">
                <?php echo $object->c_postContent; ?>
            </div>
            <div class="my-4">
                <button class="btn-light float-right mx-3 px-3" id="summerpost">Submit</button>
                <button type="button" class="btn-light float-right mx-3 px-3" id="summerclear" value="clear">Clear</button>
            </div>
        </form>
    </div>
</div>
<!-- 
loaded from header files
https://www.gstatic.com/firebasejs/6.1.0/firebase-app.js
https://www.gstatic.com/firebasejs/6.1.0/firebase-database.js
-->
<script type="text/javascript">
(function($){
  /* firebase section */
  $.fn.sortDataFirebase = function(){
    const dbReff = firebase.database().ref("feeds");
    dbReff.orderByKey();
  }

  $.fn.submitAddPost = function ($idpost, $author, $title, $content, $date) {
    var dbReff = firebase.database().ref("feeds");
    var F_IDPOST = $idpost;
    var F_AUTHOR = $author;
    var F_CONTENT = $content;
    var F_TITLE = $title;
    var F_DATE = $date;
    dbReff.push({
        "idPost": F_IDPOST,
        "author": F_AUTHOR,
        "title": F_TITLE,
        "content" : F_CONTENT,
        "postDate" : F_DATE
        }, function(error){
          if(error){
            return false; //console.log("firebase push error : " + error);
          }
        }).then(function(){
            $(this).sendFCMessage($title, $author);
        });
  };

  $.fn.submitUpdatePost = function ($idpost, $author, $title, $content, $date) {
    var F_IDPOST = $idpost;
    var F_AUTHOR = $author;
    var F_CONTENT = $content;
    var F_TITLE = $title;
    var F_DATE = $date;
    var F_POSTKEY = '';
    const idPost = "idPost"; // console.log("try to update : "+F_IDPOST);
    const dbReff = firebase.database().ref("feeds");
    dbReff.orderByChild("idPost").equalTo(F_IDPOST).once("value", function(snapshot) {
      snapshot.forEach(function(data){
        F_POSTKEY = data.key;
        if(F_POSTKEY != null || F_POSTKEY != ''){ // console.log("data key : " + F_POSTKEY);
          dbReff.child(F_POSTKEY).remove();
        }
      });
    }).then(function(){
      dbReff.push({
        "idPost": F_IDPOST,
        "author": F_AUTHOR,
        "title": F_TITLE,
        "content" : F_CONTENT,
        "postDate" : F_DATE
        }, function(error){
          if(error){
            return "there is an error submit update post"; //console.log("firebase push error : " + error);
          }
        }).then(function(){
          $(this).sendFCMessage($title, $author);
        });
    });
    return false;
  };

  $.fn.confirmSubmitFirebase = function(reff){
    var $iduser = "<?php echo $this->session->userdata['logged_in']['iduser']; ?>";
    var $postTitle = $('#postedTitle').val();
    var $postedText = $('#summernote').summernote('code');
    var $author = "<?php echo $this->session->userdata['logged_in']['username']; ?>";
    var $date = $(this).currDateTime();
    var data = new FormData();
    data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash();?>');
    data.append('iduser', $iduser);
    data.append('postedTitle', $postTitle);
    data.append('postedText', $postedText);
    data.append('reff', reff);
    $.ajax({
      url : 'V2/posting',
      type: 'POST',
      dataType: 'text', 
      cache: false, 
      contentType: false,
      processData: false, 
      data: data,
      success: function(data){}
    }).done(function(result){
      if(result != '' && result != 'exist'){
        /* firebase cloud update : console.log("add post to firebase");*/
        $(this).submitAddPost(result, $author, $postTitle, $postedText, $date);
      }
    });
  }

  $.fn.confirmUpdateFirebase = function(){
    var $idpost = $('#summerUpdate').attr('value');
    var $iduser = "<?php echo $this->session->userdata['logged_in']['iduser']; ?>";
    var $postTitle = $('#postedTitle').val();
    var $postedText = $('#summernote').summernote('code');
    var $author = "<?php echo $this->session->userdata['logged_in']['username']; ?>";
    var $date = $(this).currDateTime();
    var data = new FormData();
    data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash();?>');
    data.append('iduser', $iduser);
    data.append('idpost', $idpost);
    data.append('postedTitle', $postTitle);
    data.append('postedText', $postedText);
    $.ajax({
      url : 'V2/postingUpdate',
      type: 'POST',
      dataType: 'text', 
      cache: false, 
      contentType: false,
      processData: false, 
      data: data
    }).done(function(data){
      $(this).submitUpdatePost($idpost, $author, $postTitle, $postedText, $date);
    });
  }

  $.fn.sendFCMessage = function(title, author){
    var url = "https://fcm.googleapis.com/fcm/send";
    //var apiKey = "key=AIzaSyBa24uitsILzi7u32ZK0RJr05vJly0OM2A"; //sibep-notification
    var apiKey = "key=AIzaSyA8ozYAqOnRPYBb4kQ7ZMzoTf5RugdKfyY"; //hello-firebase
    var topics = "/topics/GLOBALPOST";
    var payload = JSON.stringify({
      'to': topics,
      'data': { 'title': title, 'author': author }
      });
    var msgid;
    $.ajax({
        url : url,
        type : 'POST',
        dataType : 'json',
        cache : false,
        ContentType:'application/json; charset=utf-8', 
        headers: {'Content-Type' : 'application/json', 'Authorization' : apiKey },
        data : payload
      }).done(function(data){
        msgid = data.message_id;
        $(this).sortDataFirebase();
        $(this).resultModalUI(msgid);
      });
  }

  /* end firebase section */
  
  $.fn.resultModalUI = function(msgid){
    var modale = $('body .modal.show');
    var modalr;
    if(msgid !== ''){
        modalr = $('#modalResponsePostSuccess .modal-content');
        modale.children().find('.modal-content').html(modalr);
        modale.on('hide.bs.modal', function(){ location.reload(); });
    }else{
        modalr = $('#modalResponsePostFail');
        modalr.on('show.bs.modal', function(){
          modalr.children().find('.modal-body').text('Title already exist');
        });
        modalr.modal('show');
    }
  }

  $.fn.loadingModalUI = function(){
    var modale = $('body .modal.show');
    modale.children().find('.modal-body').text('loading...');
  }
}(jQuery));

$(document).ready(function(){
    /* Summernote */
  $('#form-new-post').bind('contextmenu', function(e){
      return false; //console.log('right click is disabled');
    }).on('cut copy paste', function(e){
      e.preventDefault();
      return false; //console.log('disabled cut copy paste');
  });

  $('#summerpost').on('click', function(e){
    e.preventDefault();
    if($(this).validateForm()){
      $('#modalConfirmSubmitPost').modal('show');
    }
    return false;
  });

  $('#summerUpdate').on('click', function(){
    if($(this).validateForm()){
      $('#modalConfirmUpdatePost').modal('show');
    }
    return false;
  });
  
  $('#summerclear').on('click', function(){
    var note = $('body').children().find('#summernote');
    note.summernote('code','');
    $('#postedTitle').val('').removeClass('bg-danger');
  });

  /* posting */
  $('#modalConfirmSubmitPost').on('show.bs.modal', function(){
    $('#modalConfirmSubmitPost .modal-body').text('Apakah data sudah benar ?');
    var button = $('#modalConfirmSubmitPost .modal-footer').css('display') !== 'none' ? '' : $('#modalConfirmSubmitPost .modal-footer').show();
  });

  $('#btnConfirmSubmitPost').on('click', function(){
    $(this).loadingModalUI();
    $(this).confirmSubmitFirebase('publish');
  });

  $('#btnConfirmUpdatePost').on('click', function(){
    $(this).loadingModalUI();
    $(this).confirmUpdateFirebase();
  });

});
</script>