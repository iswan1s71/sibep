<?php require_once('custom-function.php'); ?>

<div class="mt-30px mb-30px" id="highlight-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-12">
                <!-- Recent Updates Widget -->
                <div id="new-updates" class="card updates recent-updated">
                <div id="updates-header" class="card-header d-flex justify-content-between align-items-center">
                    <h2 class="h5 display">
                        <a data-toggle="collapse" data-parent="#new-updates" href="#updates-box" aria-expanded="true" aria-controls="updates-box">
                            Recent Post</a>
                        </h2>
                        <a data-toggle="collapse" data-parent="#new-updates" href="#updates-box" aria-expanded="true" aria-controls="updates-box"><i class="fa fa-angle-down"></i></a>
                </div>
                <div id="updates-box" role="tabpanel" class="collapse show">
                    <ul class="news list-unstyled">
                    <!-- Item-->
                    <?php 
                        if(isset($posts) && count($posts) >= 1){
                            $maxPost = count($posts);
                            for($i=0; $i<3; $i++){ 
                                if( empty($posts[$i])) break;
                                ?>
                            <li class="d-flex justify-content-between"> 
                                <div class="left-col d-flex">
                                <div class="title"><strong><?php echo $posts[$i]['c_postTitle'];?></strong>
                                    <p><?php echo $posts[$i]['c_postContent']; ?></p>
                                </div>
                                </div>
                                <div class="right-col text-right">                                           
                                    <div class="text-center">
                                        <i class="fa 
                                            <?php 
                                            $publish=($posts[$i]['c_isPublished'] > 0 ? 'fa-globe' : 'fa-pencil-square-o');
                                            echo $publish;
                                            echo '" id=' . $posts[$i]['c_idPost']; 
                                            ?> style="color:green;">
                                        </i>
                                    </div>
                                <?php
                                if($posts[$i]['c_isModified'] == 0){
                                    echo '<div class="update-date" title="new posted">';
                                    echo date('d', strtotime($posts[$i]['c_postDate']));
                                    echo ' <span class="month">';
                                    echo date('M', strtotime($posts[$i]['c_postDate'])) . '</span></div>';
                                }else{
                                    echo '<div class="update-date" title="last edited">';
                                    echo date('d', strtotime($posts[$i]['c_postDate']));
                                    echo ' <span class="month">';
                                    echo date('M', strtotime($posts[$i]['c_postDate'])) . '</span></div>';
                                }
                                ?>
                                </div>
                            </li>
                            <?php     
                        }}
                    ?>
                    <!-- end of item -->
                    </ul>
                </div>
            </div>
        </div>
        <!-- Recent Updates Widget End-->
        <div class="col-lg-4 col-md-6">
            <!-- Daily Feed Widget-->
            <div id="widget-published-post" class="card updates daily-feeds border border-success">
            
            <div id="widget-published-post-header" class="card-header d-flex justify-content-between align-items-center">
                <h2 class="h5 display">
                    <a data-toggle="collapse" data-parent="#widget-published-post" href="#widget-published-post-box" aria-expanded="true" aria-controls="widget-published-post-box">
                        Published post </a>
                </h2>
                <div class="right-column">
                    <div class="badge badge-primary"><?php echo countPublishedPost($posts); ?> messages </div>
                    <a data-toggle="collapse" data-parent="#widget-published-post" href="#widget-published-post-box" aria-expanded="true" aria-controls="widget-published-post-box">
                        <i class="fa fa-angle-down"></i>
                    </a>
                </div>
            </div>
            <div id="widget-published-post-box" role="tabpanel" class="collapse show">
                <div class="feeds-box">
                <ul class="feed-elements list-unstyled">
                <?php 
                    if(isset($posts) && count($posts) >= 1 ){
                        $limit = 0;
                        for($i=0; $i<3; $i++){
                            if( empty($posts[$i])) break;
                            if($posts[$i]['c_isPublished'] == 1){ 
                    ?>
                    
                    <li class="clearfix">
                        <div class="feed d-flex justify-content-between tooltip-point">
                            <div class="feed-body content">
                                <strong><?php echo $posts[$i]['c_postTitle'];?></strong>
                                <small><p><?php echo $posts[$i]['c_postContent']; ?></p></small>
                            </div>

                                <?php if($posts[$i]['c_hasAttachment'] == 1){ 
                                    ?>
                                    <div class="previewpic">
                                        <a href="#" class="feed-profile">
                                            <img src="<?php echo $posts[$i]['c_urlAttachment']; ?>" 
                                            alt="images" class="img-fluid rounded-circle">
                                        </a>
                                    </div>
                                    <?php
                                } ?>
                        </div>
                        <div class="d-none">
                            <small class="lapse-day"><?php echo lapsetime(($posts[$i]['c_postDate'])); ?></small>
                            <small class="date-full"><?php echo date('d/M/Y h:m a', strtotime($posts[$i]['c_postDate'])); ?></small>
                        </div>
                    </li>
                    <?php } ?>                    
                <?php 
                    }} 
                ?>
                </ul>
                </div>
            </div>
            </div>
        </div>
        <!-- Daily Feed Widget End-->
        <div class="col-lg-4 col-md-6">
            <!-- Recent Activities Widget      -->
            <div id="recent-activities-wrapper" class="card updates activities">
            <div id="activites-header" class="card-header d-flex justify-content-between align-items-center">
                <h2 class="h5 display"><a data-toggle="collapse" data-parent="#recent-activities-wrapper" href="#activities-box" aria-expanded="true" aria-controls="activities-box">
                    Draft</a></h2>
                    <a data-toggle="collapse" data-parent="#recent-activities-wrapper" href="#activities-box" aria-expanded="true" aria-controls="activities-box"><i class="fa fa-angle-down"></i></a>
            <div class="badge badge-secondary"><?php echo countDraft($posts); ?> draft </div>
            </div>
            <div id="activities-box" role="tabpanel" class="collapse show">
                <ul class="activities list-unstyled">
                <!-- Item-->
                <?php 
                    if(isset($posts) && count($posts) >= 1){
                        $maxPost = count($posts);
                        $limit = 0;
                        for($i=0; $i<$maxPost; $i++){ 
                        if( empty($posts[$i])) break;
                        if($posts[$i]['c_isDeleted'] == 0 && $posts[$i]['c_isPublished'] == 0){
                ?>
                <li class="">
                    <div class="row">
                        <div class="col-4" style="margin-left:15px !important;">
                            <div class="date-holder text-center">
                                <i class="fa fa-clock-o text-secondary" style="margin-top:10px;"></i>
                                <div class="p-2">
                                    <p>
                                    <?php echo date('d', strtotime($posts[$i]['c_postDate'])); ?>
                                    <?php echo date('M', strtotime($posts[$i]['c_postDate'])); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-7" style="padding-left:unset !important;">
                            <div class="content">
                                <strong><?php echo $posts[$i]['c_postTitle'];?></strong>
                                <!-- <p><?php echo $posts[$i]['c_postContent']; ?></p> -->
                            </div>
                        </div>
                    </div>
                </li>
                <?php 
                    $limit++;
                    }
                    if($limit == 3) break;
                    }
                } ?>
                </ul>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
