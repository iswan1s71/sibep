<?php  
      $max = count($posts);
      for($i=0; $i<$max; $i++){
      //loop row post
    ?>
      <div class="card content mb-2" id="<?php echo 'post' . $posts[$i]['c_idPost'];?>" type="<?php echo $posts[$i]['c_isPublished']; ?>">
        <div class="card-header">
          <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <a href="#" class="ml-3 mc-hover" name="titlePost" id="<?php echo $posts[$i]['c_idPost'];?>"><?php echo $posts[$i]['c_postTitle'];?></a>
                <div class="container-fluid">

                  <?php 
                    if($posts[$i]['c_isModified'] == 0){
                      echo '<span class="text-muted smaller font-italic" 
                      title="posted ' . date('d/m/Y H:i', strtotime($posts[$i]['c_postDate'])) . '">
                      ' . date('d/m/Y', strtotime($posts[$i]['c_postDate'])) . '</span>';
                    }else{
                      echo '<span class="text-muted smaller" 
                      title="last edited ' . date('d/m/Y H:i', strtotime($posts[$i]['c_postModifiedDate'])) . '">
                      ' .date('d/m/Y', strtotime($posts[$i]['c_postModifiedDate'])) . '</span>';
                    }
                  ?>
                </div>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" id="rowPostAction">
                
                    <ul class="nav justify-content-end">
                      <li class="nav-item">
                        <a class="nav-link active" target="#">
                          <i class="menu-hover fa
                          <?php
                            $publish=($posts[$i]['c_isPublished'] > 0 ? 'fa-globe" title="published"' : 'fa-pencil-square-o" title="draft"');
                            echo $publish;
                          ?>
                          id="<?php echo $posts[$i]['c_idPost']; ?> "></i></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active" target="#"><i class="menu-hover fa fa-pencil" title="edit" id="<?php echo $posts[$i]['c_idPost']; ?>"></i></a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" target="#"><i class="menu-hover fa fa-trash del-post" title="delete" id="<?php echo $posts[$i]['c_idPost']; ?>"></i></a>
                      </li>
                    </ul>
                  
              </div>

          </div>
        </div>
      </div>
      <?php
      //end loop card post
    }
    ?>