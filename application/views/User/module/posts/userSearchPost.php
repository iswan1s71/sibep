<div class="row">
    <div class="col-12">
    <form action="" class="w-100">
        <div class="input-group mb-3">
        <input type="text" class="form-control" placeholder="search post" aria-describedby="basic-addon1" id="inputDropdownSearch">
        </div>
    </form>
    </div>
</div>

<script>

var cache = <?php 
    $n = count($fpost);
    echo '[';
    for ($i=0; $i<$n; $i++){ 
        $idPost = $fpost[$i]['c_idPost']; 
        $judul  = $fpost[$i]['c_postTitle']; 
        echo '{ "value":"' . $idPost . '", "label":"' . $judul .'"},'; 
        }
    echo ']';    
    ?>;



$(function(){
    $("#inputDropdownSearch").autocomplete({
        source: cache,
        focus: function(event, ui) {
            event.preventDefault();
            },
        select: function(event, ui){
            event.preventDefault();
            }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $('<li class="ui-menu-item-with-icon"></li>')
            .data("item.autocomplete", item)
            .append('<a href="'+item.value+'" class="ui-link">'+item.label+'</a><i class="fa fa-pencil abs-right" id='+item.value+'></i>')
            .appendTo(ul);
    };
    $('.ui-autocomplete').on('click', 'a', function(e){
        e.preventDefault();
        var id = $(this).attr('href');
        $(this).showPostBySearch(id);
    });
    $('.ui-autocomplete').on('click', 'i', function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        $(this).ajaxEditPost(id);
    });
});

</script>
