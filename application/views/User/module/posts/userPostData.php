<?php
if(!isset($maxPost)){
  echo '<h1 class="text-danger"> no posts coyy</h1>';
  return false;
}else{

  ?>
  <div class="mb-3">
      <?php include('userSearchPost.php'); ?>
  </div>
  <div class="" id="list_post">
      <?php include('listPostData.php'); ?>
  </div>
  <div class="container mt-5">
    <nav aria-label="Page navigation example" id="pagination_nav">
      <ul class="pagination justify-content-center">
        <li class="page-item disabled" id="pagination_prev">
          <a class="page-link" href="#" tabindex="-1">Previous</a>
        </li>
        <?php
        $pageTotal = ceil($maxPost/10);
          for($p=1; $p<=$pageTotal; $p++){
          $isActive = ($p == 1) ? 'active' : '';
          $isLast = ($p == $pageTotal) ? 'last' : '';
          echo '<li class="' . $isActive . ' page-item ' . $isLast . '"><a class="page-link" href="#">' . $p . '</a></li>';
          }
        ?>
        <li class="page-item" id="pagination_next">
          <a class="page-link" href="#">Next</a>
        </li>
      </ul>
    </nav>
    <?php
}

?>
<script type="text/javascript">

(function($){

  /* firebase section */
  $.fn.delNodeFirebase = function(idpost){
    if(idpost == '') return false;
    const dbReff = firebase.database().ref("feeds");
    dbReff.orderByChild("idPost").equalTo(idpost).once("value", function(snapshot) {
      snapshot.forEach(function(data){
        F_POSTKEY = data.key;
        if(F_POSTKEY != null || F_POSTKEY != ''){ // console.log("data key : " + F_POSTKEY);
          dbReff.child(F_POSTKEY).remove();
        }
      });
    })
  }

  $.fn.moveToArchieveNodes = function(idpost){
    if(idpost == '') return false;

    const dbReff = firebase.database().ref("feeds");
    const dbArch = firebase.database().ref("archieves");

    dbReff.orderByChild("idPost").equalTo(idpost).once("value", function(snapshot) {
      snapshot.forEach(function(data){
        F_POSTKEY = data.key;
        if(F_POSTKEY != null || F_POSTKEY != ''){ // console.log("data key : " + F_POSTKEY);
          var nodes = dbReff.child(F_POSTKEY);
          dbArch.set(snapshot.val()).then(dbReff.child(F_POSTKEY).remove());
        }
      });
    })
  }

  /* end firebase section */

  $.fn.paginationCurrent = function(){
    var current = $('#pagination_nav ul > li.active');

      if(current.text() == '1'){
        $('#pagination_prev').addClass('disabled');
      }else{
        $('#pagination_prev').removeClass('disabled');
      }

      if(current.hasClass('last')){
        $('#pagination_next').addClass('disabled');
      }else{
        $('#pagination_next').removeClass('disabled');
      }

  }

  $.fn.fetchPostResult = function(page){
    var msg = '<div class="col-12 mx-auto"><h6 class="text-success text-center">updating post, please wait</h5></div>';
    $.ajax({
      url:'V2/fetchPost',
      type: 'get',
      data:{ 'page' : page },
      beforeSend: function(){
        $('#list_post').html(msg);
      },
      success: function(data){
        $('#list_post').html(data);
      }
    }).done(function(){
      $(this).rowPostHover();
    });
  }

  $.fn.rowPostHover = function(){
  /* menu hover edit del */
    $('.content .card-header').each(function(){
      $(this).hover(function(){
        $(this).children().find('.row-post-menu').show();
        $(this).children().find('.row-post-date').css('visibility','hidden');
      }, function(){
        $(this).children().find('.row-post-menu').hide();
        $(this).children().find('.row-post-date').css('visibility','visible');
      });
    });
  }

  $.fn.ajaxEditPost = function(idpost){
    $.ajax({	
        url: 'V2/initEditingPost',
        data: {'idPost' : idpost },
        type: 'get',
        dataType: 'text',
        success: function(data){
            $('#changeable').html(data);
          }
      }).done(function(data){
            $('#summernote').summernote({
              dialogsInBody: true,
              toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontnames', ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['height', ['height']],
                ['insert', ['link', 'picture', 'hr']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['misc', ['undo', 'redo','fullscreen']],
              ],
              placeholder: 'write something...',
              height: 300,
              // callbacks: {
              //   onImageUpload: function(data){
              //     data.pop();
              //   },
              //   onPaste: function(e){
              //     e.preventDefault();
              //     return false;
              //   }
              // }
            });
            $('#summerpost').attr({'id':'summerUpdate', 'value':idpost});
      });
  }

}(jQuery));


$(document).ready(function(){

  $(this).fetchPostResult();

  $('#cAllPost').on('click', 'a[name=titlePost]', function(){
    var idpost = $(this).attr('id');
    $.ajax({
      url: 'v2/getPostById',
      data: {'idPost' : idpost },
      type: 'GET',
      dataType:'JSON',
      success: function(data){
        $('#modalSearchCardTitle').text(data.c_postTitle);
        $('#modalSearchCardContent').html(data.c_postContent);
        $('#modalSearchCardAuthor').text(data.c_nama);
        var modified = (data.c_isModified == 1)
          ? $('#modalSearchCardCite').text($(this).my_date_format(data.c_postModifiedDate))
          : $('#modalSearchCardCite').text($(this).my_date_format(data.c_postDate))
        $('#modalSearchCard').modal('show');
        $('#modalSearchCard').on('hidden.bs.modal', function(){
          $('#inputDropdownSearch').val('');
        });
      }
    });
  });

  $('#cAllPost').on('click', 'i.fa-pencil', function(){
      var idpost = $(this).attr('id');
      console.log(idpost);
      $(this).ajaxEditPost(idpost);
  });

  $('#cAllPost i.fa-globe').each(function(){});

  $('#cAllPost').on('click', 'i.fa-trash', function(){

      var title = $(this).parentsUntil('.card-header').children().find('a[name=titlePost]').text();
      console.log('title : ' + title);
      var idpost = $(this).attr('id');

      // $(this).moveToArchieveNodes(idpost);
      // return false;

      $('#modalConfirmRemovePost').on('show.bs.modal', function(){
        $(this).children().find('#titlePost').text(title);
      }).modal('show');
      $('#btnConfirmRemovePost').on('click', function(e){
        e.preventDefault();
        $.ajax({
          url: 'V2/removepost',
          dataType: 'text',
          tyep: 'GET',
          data: {'idPost' : idpost}
        }).done(function(data){
          $(this).delNodeFirebase(idpost);
          $(this).ajaxRemovePostModalResponse(data);
        });
      });
  });    

  $('#pagination_nav').on('click', 'a', function(e){
    e.preventDefault();
    var current = $('#pagination_nav ul > li.active').text();

    var page = $(this).text();
    if(page === 'Previous'){
      page = parseInt(current)-1 ;
    } 
    if(page === 'Next'){
      page = parseInt(current)+1 ;
    }

    $(this).fetchPostResult(page);

    $('#pagination_nav ul li').each(function(){
      $(this).removeClass('active');
    });

    $('#pagination_nav ul li:contains('+page+')').addClass('active');

    $(this).paginationCurrent();
  });

});

</script>