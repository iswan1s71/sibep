<?php 
  include('headers/header.php'); 
  //var_dump($_SESSION);
?>

<body>
<?php 
/* load modal here before body-home.php */
include('bodies/Modal.php'); 
?>
<div class="row mx-auto" id="loadingDiv">
  <h5 class="container text-center">loading please wait... <img src="<?php echo base_url('assets/images/loading.gif'); ?>"></h5>
</div>
<!-- new navbar -->
<?php include('bodies/navbar-top.php'); ?>
<div class="body-wrapper">
  <?php include('bodies/navbar-sideleft.php'); ?>
  <?php //include('bodies/navbar-sideleft-small.php'); ?>
  <!-- section menu home -->
  <section id="menu-home">
    <?php include('bodies/body-home.php'); ?>
    <section id="menu-post" style="display: block;"></section>
  </section>
</div>
</body>
</html>

<script type="text/javascript">

(function($){

$.fn.showPostBySearch = function(idpost){
  var form_data = new FormData();
  form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>','<?php echo $this->security->get_csrf_hash();?>');
  form_data.append('idpost', idpost);
  $.ajax({	url: 'Auth/showPostBySearch',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
              $('#modalSearchCardTitle').text(data.c_postTitle);
              $('#modalSearchCardContent').html(data.c_postContent);
              $('#modalSearchCardAuthor').text(data.c_nama);
              var modified = (data.c_isModified == 1)
                ? $('#modalSearchCardCite').text($(this).my_date_format(data.c_postModifiedDate))
                : $('#modalSearchCardCite').text($(this).my_date_format(data.c_postDate))
              $('#modalSearchCard').modal('show');
              $('#modalSearchCard').on('hidden.bs.modal', function(){
                $('#inputDropdownSearch').val('');
              });
            }
    }).done(function(data){
      //$('#modalSearchCardContent').summernote('pasteHTML', data.c_postContent);
    });
};

}(jQuery));


$('#loadingDiv').show();

$(document).ready(function(){
  var leftmenu = $('#navbarToggleExternalContent');
  var menuhome = $('#menu-home');
  var button = $('#btn-navbarToggleExternalContent');

  button.on('click', function(e){
    e.preventDefault();
    leftmenu.toggle('slide', function(){
      menuhome.toggleClass('compact')
    });
  });

  var fullText = null;

  $.ajax({
    url: 'Auth/getPublicPostLatest',
    dataType: "json",
    success: function(data){
      var html = '';
      $.each(data, function(key, value){
        html += '<div class="card content p-3" id='+value.c_idPost+'>';
        if(value.c_urlAttachment != ''){
          html += '<img class="card-img-top" src="'+value.c_urlAttachment+'">';
        }
        html += '<div class="card-body -pb mc-hover-white">';
        html += '<h5 class="card-title font-weight-bold">'+value.c_postTitle+'</h5>';
        html += '<div class="card-text" name="shortContent">'+value.c_postContent+'</div><hr>';
        html += '<div class="d-none" name="fullContent">'+value.c_postContent+'</div>';
        html += '<footer class="blockquote-footer">';
        html += '<span class="text-muted mr-2" title="author">'+value.c_divisi+', </span>';
        html += '<span class="text-muted font-italic" title="author">'+value.c_nama+' </span></br>';
        if(value.c_isModified == 0){
            html += '<cite>posted '+$(this).my_date_format(value.c_postDate)+'</cite>';
        }else{
            html += '<cite>updated '+$(this).my_date_format(value.c_postModifiedDate)+'</cite>';
        }
        html += '</footer></div></div>';
      });
      $('#postBox').html(html);
    }
  }).done(function(){
    $('#postBox div.d-none[name=fullContent]').each(function(){
      $(this).siblings('[name=shortContent]').ellipsis({ row:3 });
    });
    $(this).clickableModal2();
  });

  $('#pageFooterUser').fadeIn(3000);
  //$('#pageFooterUser').append('<h1>'+$(this).currDateTime()+'</h1>');

});

</script>